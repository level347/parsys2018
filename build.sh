tar -xvzf "$1.tar.gz"
cd "./$1"
mkdir -p cmake-build-release
cd ./cmake-build-release
cmake -DCMAKE_BUILD_TYPE=Release ../
make

