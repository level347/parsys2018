ssh-keygen -t rsa -C $1 -f "$1"
scp "$1.pub" "$1@lcc2.uibk.ac.at:/home/cb01/$1/authorized_keys"
ssh "$1@lcc2" "mkdir -p .ssh && mv ./authorized_keys ./.ssh/authorized_keys && chmod 0700 ./.ssh && chmod 0600 ./.ssh/authorized_keys"
