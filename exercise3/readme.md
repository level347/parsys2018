#Exercise 3

##Results

The results of assembly output analysis in regard to auto-vectorization is shown below. Every
cell contains a boolean value that indicates if a compiler vectorized the most inner loop of the
corresponding matrix multiplication. The integer value next to it specifies the loop unroll count.


| Version\Compiler  |   GCC 8.2  |   Clang 7.0  |  ICC 19  |  MSVC 19  |
|-------------------|:-----:|:-----:|:-----:|:-----:|
| C static int   |  no/1 |  yes/32 |   yes/8  |   yes/16   |
| C static float   |  no/1 |  yes/32 |   yes/8  |   yes/16   |
| C static double | no/1 | yes/16 | yes/4 | yes/8 |
| C dynamic int   |  no/1 |  yes/32 | yes/8 |  no/1   |
| C dynamic float   |  no/1 |  yes/32 | yes/8 |   no/4   |
| C dynamic double | no/1 | yes/16 | yes/4 | no/4 |
| C++ dynamic int   |  no/1 |  no/2 |   yes/8  |   no/1   |
| C++ dynamic float   |  no/1 |  no/2 |   yes/8  |   no/4   |
| C++ dynamic double  | no/1 | no/2 | yes/4 | no/4 |

##Discussion

The results show that the static C version of the matrix multiplication was auto-vectorized by 3/4 compilers. The
dynamic C version was still vectorized by two compilers. The C++ version was only vectorized by one compiler. Interestingly
GCC was not able to vectorize any version and also did not perform any loop unrolling. Clang unrolled the loops with a
higher unroll factor than the other compilers if it was able to vectorize the loop. All compilers halved the unroll 
factor in two if the double data type was used in vectorized loops. This makes sense, since the instruction size determines
the upper boarder of the unroll count, which is the same because the data type size doubles and unroll count halves.
