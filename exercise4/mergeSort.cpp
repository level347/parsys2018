#include <iostream>
#include <random>
#include <cstring>
#include <algorithm>
#include <omp.h>

#include "mergeSort.h"

static std::mt19937 gen (123);
static std::uniform_real_distribution<double> distrib(0.0, 1.0);

constexpr int treshold = 1000;

void fillRandomized(VALUE* array, int size) {

    for(int idx=0; idx<size; idx++) {
        array[idx] = distrib(gen);
    }
}

bool isSorted(VALUE* array, int size) {
    for(int idx=1;  idx<size; idx++) {
        if (array[idx-1]>array[idx]) {
            return false;
        }
    }
    return true;
}

void mergeSerial(VALUE* array, int size, VALUE* tmp) {
    int idx = 0;
    int jdx = size/2;
    int tdx = 0;

    while (idx < size/2 && jdx < size) {
        if(array[idx] < array[jdx]) {
            tmp[tdx] = array[idx];
            idx++;
        }
        else {
            tmp[tdx] = array[jdx];
            jdx++;
        }
        tdx++;
    }
    while(idx < size/2) {
        tmp[tdx] = array[idx];
        idx++;
        tdx++;
    }
    while (jdx < size) {
        tmp[tdx] = array[jdx];
        jdx++;
        tdx++;
    }

    memcpy(array, tmp, sizeof(VALUE)*size);

}

void mergeSortSerial(VALUE* array, int size, VALUE* tmp) {
    if(size <= treshold) {
        std::sort(array, &array[size]);
        return;
    }
    mergeSortSerial(array, size/2, tmp);
    mergeSortSerial(&array[size/2], size-(size/2), tmp);

    mergeSerial(array, size, tmp);
}

void mergeSortParallel(VALUE* array, int size, VALUE* tmp, int threadCount) {
    if(threadCount == 1) {
        mergeSortSerial(array, size, tmp);
        return;
    }
#pragma omp parallel sections
    {
#pragma omp section
        mergeSortParallel(array, size/2, tmp, threadCount/2);
#pragma omp section
        mergeSortParallel(&array[size/2], size - (size/2), &tmp[size/2], threadCount - threadCount/2);
    };

    mergeSerial(array, size, tmp);
}

void mergeSortSerial(VALUE* array, int size) {
    VALUE* tmp = new VALUE[size];
    mergeSortSerial(array, size, tmp);
}

void mergeSortParallel(VALUE* array, int size) {
    VALUE* tmp = new VALUE[size];
    mergeSortParallel(array, size, tmp, omp_get_max_threads());
}