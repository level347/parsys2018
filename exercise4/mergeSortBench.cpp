#include <iostream>

#include "mergeSort.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    if(argc != 2) {
        return EXIT_FAILURE;
    }
    int size = atoi(argv[1]);

    VALUE* array = new VALUE[size];
    fillRandomized(array, size);
    {
        ChronoTimer timer("Serial Merge Sort");
        mergeSortSerial(array, size);
    }
    if(isSorted(array, size)) {
        std::cout << "Array is sorted. " << std::endl;
    }
    else {
        std::cout << "Array is not sorted. " << std::endl;
    }

    fillRandomized(array, size);
    {
        ChronoTimer timer("Parallel Merge Sort");
        mergeSortParallel(array, size);
    }
    if(isSorted(array, size)) {
        std::cout << "Array is sorted. " << std::endl;
    }
    else {
        std::cout << "Array is not sorted. " << std::endl;
    }
}