
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <atomic>
#include <omp.h>

#include "chrono_timer.h"

constexpr uint8_t maxLength = 32;

struct Positions {
    int8_t posX[maxLength];
    int8_t posY[maxLength];
    uint8_t length;

    void push_back(int8_t x, int8_t y) {
        posX[length] = x;
        posY[length] = y;
        length++;
    }
};

bool isNotConflicting(int8_t x, int8_t y, const Positions& positions) {
    bool conflict = false;
    for(int idx = 0; idx < positions.length; idx++) {
        conflict = (y == positions.posY[idx]);
        conflict |= (abs(y - positions.posY[idx]) == (x - positions.posX[idx]));
        if(conflict) {
            return false;
        }
    }
    return true;
}


uint64_t nQueensSerial(int8_t n, int8_t x, Positions& positions) {
    if(x == n) {
        return 1;
    }
    uint64_t solution = 0;
    for(int8_t i = 0; i < n; i++) {
        if(isNotConflicting(x, i, positions)) {
            Positions newPositions = positions;
            newPositions.push_back(x,i);
            solution += nQueensSerial(n, x+1, newPositions);
        }
    }
    return solution;
}


uint64_t nQueensSerial(int n) {
    if(n == 0) {
        return 0;
    }
    Positions positions = {};
    return nQueensSerial(n, 0, positions);
}

uint64_t nQueensParallel(int n) {
    if(n == 0) {
        return 0;
    }
    else if(n == 1) {
        return 1;
    }

    uint64_t solutions = 0;

    #pragma omp parallel for schedule(dynamic), reduction(+:solutions)
    for(int idx = 0; idx < n; idx++) {
        Positions positions = {};
        positions.push_back(0, idx);
        solutions += nQueensSerial(n, 1, positions);
    }

    return solutions;
}