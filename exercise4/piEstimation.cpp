
#include <random>
#include <omp.h>

double estimatePiParallel(int samples, int seed) {

    int inside = 0;

    #pragma omp parallel
    {
        // one random number generator per thread
        std::mt19937 gen ((unsigned long)seed+(unsigned long)omp_get_thread_num());
        std::uniform_real_distribution<double> distrib(0.0, 1.0);


        #pragma omp for reduction(+:inside)
        for(int idx = 0; idx < samples; idx++){
            double x = distrib(gen);
            double y = distrib(gen);
            if(sqrt(pow(x,2.0)+pow(y,2.0)) <= 1.0) {
                inside++;
            }
        }

    };

    return (double)inside/(double)samples*4.0;
}

double estimatePiSerial(int samples, int seed) {
    std::mt19937 gen (seed);
    std::uniform_real_distribution<double> distrib(0.0, 1.0);

    int inside = 0;
    for(int idx = 0; idx < samples; idx++){
        double x = distrib(gen);
        double y = distrib(gen);
        if(pow(x,2.0)+pow(y,2.0) <= 1.0) {
            inside++;
        }
    }
    return (double)inside/(double)samples*4.0;
}
