#ifndef EXERCISE4_NQUEENS_H
#define EXERCISE4_NQUEENS_H

#include <cstdint>

uint64_t nQueensSerial(int n);
uint64_t nQueensParallel(int n);

#endif //EXERCISE4_NQUEENS_H
