
#include "nQueens.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    if(argc != 2) {
        return EXIT_FAILURE;
    }
    int n = atoi(argv[1]);

    uint64_t solution = 0;
    {
        ChronoTimer timer("Serial N-Queens");
        solution = nQueensSerial(n);
    }
    std::cout << "Solution: " << solution << std::endl;

    {
        ChronoTimer timer("Parallel N-Queens");
        solution = nQueensParallel(n);
    }
    std::cout << "Solution: " << solution << std::endl;
}

