
#include "piEstimation.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    if(argc != 2) {
        return EXIT_FAILURE;
    }
    int samples = atoi(argv[1]);

    std::cout.precision(17);
    double piEstimation = 0.0;
    {
        ChronoTimer timer("Serial Pi Estimation");
        piEstimation = estimatePiSerial(samples, 107);
    }
    std::cout << "Estimated Pi value: " << piEstimation << std::endl;

    piEstimation = 0.0;
    {
        ChronoTimer timer("Parallel Pi Estimation");
        piEstimation = estimatePiParallel(samples, 107);
    }
    std::cout << "Estimated Pi value: " << piEstimation << std::endl;

}

