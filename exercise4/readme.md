#Exercise 4

##Benchmark Regimen

###Problem Sizes

To determine the largest problem size we used one second as execution time target per run. For the mergesort
and the pi estimation we halved the problem sizes successively. In the case of N-Queens we iteratively decreased
it by one to receive useful problem sizes.

####Merge Sort

* 10,000,000
* 5,000,000
* 2,500,000
* 1,250,000
* 625,000

####NQueens

* 14
* 13
* 12
* 11
* 10

####PiEstimation

* 40,000,000
* 20,000,000
* 10,000,000
* 5,000,000
* 2,500,000

###Software Configuration

* Virtual Machine: VMWare 14.1.1
* OS: Linux Mint 19 Tara
* Compiler: gcc 7.3.0
* OpenMP: 4.5
* Target: x86_64-linux-gnu

###Hardware Platforms

* System 1: Intel i7 4770K @ 3.9GHz - 4 cores - 6GB
* System 2: AMD Ryzen 7 1700 @ 3.7GHz - 8 cores - 6GB

This list states the memory assigned to the virtual machine, not the physical memory.


##Benchmark Results

Execution time is specified in milliseconds. All benchmarks were repeated 3 times. The median value was used.
For timing, the chrono timer was used.

###Merge Sort

####System 1

|            | 625K | 1.25M | 2.5M | 5M   | 10M  |
|------------|------|-------|------|------|------|
| Sequential | 54   | 115   | 240  | 510  | 1084 |
| Parallel   | 29   | 63    | 133  | 282  | 593  |
| Speedup    | 186% | 182%  | 180% | 181% | 183% |

####System 2

|            | 625K | 1.25M | 2.5M | 5M   | 10M  |
|------------|------|-------|------|------|------|
| Sequential | 71   | 152   | 323  | 678  | 1446 |
| Parallel   | 39   | 82    | 174  | 363  | 765  |
| Speedup    | 182% | 185%  | 186% | 187% | 189% |

###NQueens

####System 1

|            | 10   | 11    | 12   | 13  | 14   |
|------------|------|-------|------|-----|------|
| Sequential | 3   | 19   | 109  | 640 | 4067 |
| Parallel   | 1   | 5    | 29  | 188 | 1143  |
| Speedup    |   300%   |  380%     |  375%    |  340%   |   355%   |

####System 2

|            | 10   | 11    | 12   | 13   | 14   |      
|------------|------|-------|------|------|------|
| Sequential | 4    | 23    | 130  | 770  | 4879 |
| Parallel   | 1    | 4     | 22   | 127  | 776  |
| Speedup    | 400% | 575%  | 590% | 606% | 628% |

###PiEstimation

####System 1

|            | 2.5M | 5M | 10M | 20M  | 40M  |
|------------|------|-------|------|-----|------|
| Sequential | 76   | 153   | 306  | 607 | 1218 |
| Parallel   | 21   | 43    | 83  | 167 | 325  |
| Speedup    |  362%    |   355%    |   369%   |  363%   |  375%    |

####System 2

|            | 2.5M | 5M | 10M | 20M  | 40M  |
|------------|------|-------|------|-----|------|
| Sequential | 100   | 201 | 403  | 804 | 1610 |
| Parallel   | 16   | 31    | 60  | 130 | 240  |
| Speedup    |  625%    |   648%    |   671%   |  618%   |  671%  |
