#Benchmark Regimen

##Problem Sizes
The largest problem sizes was selected by taking the maximum acceptable benchmark duration (1 min) and dividing it by 
the number of repetitions (10). The other problem sizes were deduced by halfing the cubic problem size and calculating 
the cubic root of the result iteratively, which halves the number of necessary operations in this example.
* 400
* 500
* 630
* 800
* 1000
* 1800 (only System 2 to show scaling)

##Software Configuration
* Virtual Machine: VMWare 14.1.1
* OS: Linux Mint 19 Tara
* Compiler: gcc 7.3.0
* Target: x86_64-linux-gnu

##Hardware Platforms
* System 1: Intel i7 4770K @ 3.9GHz - 4 cores - 6GB
* System 2: AMD Ryzen 7 1700 @ 3.7GHz - 8 cores - 6GB

This list states the memory assigned to the virtual machine, not the physical memory.

##Metrics
* real time
* user time
* system time
* max resident size
* cpu utilization

##Error Reduction
* The benchmarks are repeated 10 times on System 1 and 3 times on System 2. 
  The reason for this is to keep the execution time acceptable for the additional problem size.
* Between iterations, short pauses are introduced to ensure the CPU uses its maximum boost clock during the benchmark runs.
* Repetitions with same problem sizes are not executed consecutively. Instead, they are permuted with repetitions of 
  other problem sizes, so that varying background noise does not influence one problem size exceedingly.
* Mean, median, min values, max values and standard deviation are given for real, user and system time results to show
  the steadiness of the measurements.
* The maximum resident size and CPU utilization are measured over all problem sizes for this example.


#Serial Optimization

##Tiling
Multiplying large matrices results in a great number of cache evictions. To localize cache accesses and prevent evictions,
the matrix is tiled into smaller pieces.

##Transposing
With this optimization the second matrix is transposed before multiplying the matrices. The multiplication itself accesses
the second matrix with switched indices, to ensure the same operation as before is performed. Now both matrices are accessed
through iteration over the rows, which elements are stored consecutively in memory.

#OpenMP

The original example was annotated with a #pragma omp parallel for directive before the outer for loop.

#Results

##Summary
The table below shows a summary of the benchmark results. The real, user and sys means are given for the specified problem
sizes.

System 1: n = 1000

|            | CPU utilization | Max resident size | real mean | user mean | sys mean |
|------------|-----------------|-------------------|-----------|-----------|----------|
| original   |       99%       |       26220       |   5.331   |   5.321   |   0.009  |
| tiled      |       99%       |       26192       |   3.000   |   2.989   |   0.011  |
| transposed |       99%       |       34108       |   2.873   |   2.858   |   0.014  |
| openMP     |       384%      |       26876       |   1.341   |   5.206   |   0.020  |

System 2: n = 1800

|            | CPU utilization | Max resident size | real mean | user mean | sys mean |
|------------|-----------------|-------------------|-----------|-----------|----------|
| original   |       99%       |       78652       |   34.486  |   34.456  |   0.028  |
| tiled      |       99%       |       78636       |   27.591  |   27.565  |   0.023  |
| transposed |       99%       |       103964      |   27.601  |   27.566  |   0.031  |
| openMP     |       726%      |       79504       |   5.140   |   37.750  |   0.069  |

##Discussion

The CPU utilization behaves as expected. All 3 serial versions are limited to one core and therefore 100% utilization,
which they can nearly achieve. The OpenMP version is able to use all cores and comes quite close to the theoretical limit
of 100% * core count utilization.

The maximum resident size is the same for all variants, except the one using the transposed matrix. This is of course
because the transposed variant needs space for an extra matrix.

The original example shows a linear dependency between the cubic problem size and the real time for the first three 
problem sizes. Problem size 800 and above show an abnormal growth in execution time. The most logical explanation would be
cache eviction do to the larger matrices.

The tiled variation does not show this behaviour and scales linearly as expected. This substantiates the assertion that
cache eviction is the problem in the original example. However, the advantage of this variant is only visible with larger
problem sizes. Smaller problem sizes are executed substantially slower, possibly due to decreased consecutive memory accesses.

The version that uses a transposed matrix and tiling is minimal faster than the tiled version on System 1. On System 2 this optimization has nearly no effect. 
This might indicate, that the column based indexing of the second matrix was not the bottleneck.

The OpenMP variation is based on the original example and faces the same scaling problem. However, since it is executed in
parallel by multiple threads it is much faster. It scales well with the number of cores used in the benchmarks.

Deviations are unremarkable for all runs. The system time is as expected nearly zero, since neither I/O nor other context 
switching actions are performed during the matrix multiplication.

##Detailed Results System 1

###Original

n = 400

                Mean        Std.Dev.    Min         Median      Max
    real        0.086       0.001       0.084       0.085       0.088       
    user        0.083       0.003       0.080       0.083       0.088       
    sys         0.002       0.002       0.000       0.004       0.004       

n = 500

                Mean        Std.Dev.    Min         Median      Max
    real        0.172       0.002       0.170       0.171       0.175       
    user        0.170       0.003       0.163       0.171       0.173       
    sys         0.002       0.003       0.000       0.000       0.008       

n = 630

                Mean        Std.Dev.    Min         Median      Max
    real        0.341       0.005       0.335       0.339       0.353       
    user        0.335       0.004       0.327       0.336       0.344       
    sys         0.005       0.006       0.000       0.004       0.020       

n = 800

                Mean        Std.Dev.    Min         Median      Max
    real        0.892       0.031       0.841       0.886       0.966       
    user        0.886       0.032       0.833       0.886       0.962       
    sys         0.006       0.004       0.000       0.006       0.012       

n = 1000

                Mean        Std.Dev.    Min         Median      Max
    real        5.331       0.060       5.274       5.323       5.493       
    user        5.321       0.061       5.254       5.311       5.484       
    sys         0.009       0.004       0.004       0.008       0.020

###Tiled

n = 400

            Mean        Std.Dev.    Min         Median      Max
    real        0.171       0.001       0.168       0.171       0.172       
    user        0.167       0.002       0.163       0.167       0.171       
    sys         0.004       0.002       0.000       0.004       0.008       

n = 500

            Mean        Std.Dev.    Min         Median      Max
    real        0.343       0.003       0.338       0.344       0.348       
    user        0.339       0.004       0.330       0.339       0.344       
    sys         0.004       0.003       0.000       0.004       0.008       

n = 630

            Mean        Std.Dev.    Min         Median      Max
    real        0.705       0.003       0.698       0.705       0.709       
    user        0.700       0.004       0.694       0.701       0.705       
    sys         0.005       0.003       0.000       0.004       0.012       

n = 800

            Mean        Std.Dev.    Min         Median      Max
    real        1.501       0.009       1.487       1.501       1.522       
    user        1.495       0.009       1.483       1.496       1.518       
    sys         0.006       0.003       0.004       0.004       0.012       

n = 1000

            Mean        Std.Dev.    Min         Median      Max
    real        3.000       0.058       2.970       2.979       3.174       
    user        2.989       0.061       2.959       2.967       3.169       
    sys         0.011       0.004       0.004       0.010       0.016

###Transposed

n = 400

                Mean        Std.Dev.    Min         Median      Max
    real        0.165       0.002       0.163       0.165       0.171       
    user        0.163       0.004       0.155       0.163       0.171       
    sys         0.002       0.003       0.000       0.000       0.008       

n = 500

            Mean        Std.Dev.    Min         Median      Max
    real        0.330       0.001       0.328       0.330       0.332       
    user        0.327       0.003       0.321       0.328       0.330       
    sys         0.003       0.003       0.000       0.004       0.008       

n = 630

            Mean        Std.Dev.    Min         Median      Max
    real        0.681       0.003       0.678       0.680       0.687       
    user        0.674       0.004       0.669       0.675       0.683       
    sys         0.006       0.004       0.000       0.006       0.012       

n = 800

            Mean        Std.Dev.    Min         Median      Max
    real        1.431       0.005       1.420       1.431       1.436       
    user        1.424       0.005       1.416       1.426       1.429       
    sys         0.007       0.005       0.000       0.006       0.016       

n = 1000

            Mean        Std.Dev.    Min         Median      Max
    real        2.873       0.053       2.844       2.852       3.029       
    user        2.858       0.051       2.832       2.841       3.009       
    sys         0.014       0.007       0.004       0.012       0.024

###OpenMP

n = 400

                Mean        Std.Dev.    Min         Median      Max
    real        0.027       0.001       0.024       0.027       0.029   
    user        0.085       0.004       0.080       0.085       0.094       
    sys         0.007       0.003       0.004       0.007       0.011       

n = 500

                Mean        Std.Dev.    Min         Median      Max           
    real        0.048       0.003       0.046       0.047       0.057    
    user        0.169       0.004       0.160       0.169       0.176     
    sys         0.007       0.005       0.000       0.004       0.015    
   

n = 630

                Mean        Std.Dev.    Min         Median      Max
    real        0.097       0.005       0.091       0.097       0.107       
    user        0.346       0.010       0.325       0.349       0.359       
    sys         0.008       0.003       0.004       0.008       0.015       

n = 800

                Mean        Std.Dev.    Min         Median      Max
    real        0.224       0.011       0.209       0.223       0.244       
    user        0.816       0.038       0.778       0.804       0.890       
    sys         0.015       0.009       0.008       0.010       0.032       

n = 1000

                Mean        Std.Dev.    Min         Median      Max
    real        1.341       0.017       1.323       1.334       1.378       
    user        5.206       0.043       5.146       5.199       5.299       
    sys         0.020       0.006       0.008       0.020       0.028
    
##Detailed Results System 2

###Original

n = 400

                Mean        Std.Dev.    Min         Median      Max
    real        0.072       0.000       0.072       0.072       0.072       
    user        0.069       0.002       0.067       0.067       0.072       
    sys         0.003       0.002       0.000       0.004       0.004       

n = 500

                Mean        Std.Dev.    Min         Median      Max
    real        0.141       0.001       0.140       0.140       0.141       
    user        0.136       0.001       0.136       0.136       0.137       
    sys         0.004       0.000       0.004       0.004       0.004       

n = 630

                Mean        Std.Dev.    Min         Median      Max
    real        0.275       0.001       0.274       0.275       0.275       
    user        0.270       0.003       0.267       0.271       0.273       
    sys         0.004       0.003       0.000       0.004       0.008       

n = 800

                Mean        Std.Dev.    Min         Median      Max
    real        0.582       0.009       0.573       0.580       0.593       
    user        0.578       0.009       0.568       0.575       0.589       
    sys         0.004       0.000       0.004       0.004       0.004       

n = 1000

                Mean        Std.Dev.    Min         Median      Max
    real        1.344       0.016       1.329       1.337       1.367       
    user        1.339       0.014       1.325       1.333       1.359       
    sys         0.005       0.002       0.004       0.004       0.008       

n = 1800

                Mean        Std.Dev.    Min         Median      Max
    real        34.486      0.161       34.259      34.574      34.624      
    user        34.456      0.164       34.225      34.551      34.590      
    sys         0.028       0.006       0.020       0.032       0.032  



###Tiled

n = 400

                Mean        Std.Dev.    Min         Median      Max
    real        0.224       0.001       0.223       0.223       0.225       
    user        0.221       0.003       0.219       0.219       0.225       
    sys         0.003       0.002       0.000       0.004       0.004       

n = 500

                Mean        Std.Dev.    Min         Median      Max
    real        0.478       0.003       0.475       0.479       0.481       
    user        0.475       0.004       0.470       0.475       0.480       
    sys         0.003       0.002       0.000       0.004       0.004       

n = 630

                Mean        Std.Dev.    Min         Median      Max
    real        1.011       0.001       1.010       1.010       1.012       
    user        1.007       0.004       1.002       1.006       1.012       
    sys         0.004       0.003       0.000       0.004       0.008       

n = 800

                Mean        Std.Dev.    Min         Median      Max
    real        2.160       0.002       2.157       2.160       2.162       
    user        2.158       0.001       2.157       2.157       2.160       
    sys         0.001       0.002       0.000       0.000       0.004       

n = 1000

                Mean        Std.Dev.    Min         Median      Max
    real        4.359       0.004       4.355       4.357       4.364
    user        4.349       0.008       4.339       4.349       4.358
    sys         0.009       0.005       0.004       0.008       0.016

n = 1800

                Mean        Std.Dev.    Min         Median      Max
    real        27.591      0.040       27.536      27.610      27.628      
    user        27.565      0.042       27.506      27.585      27.603      
    sys         0.023       0.005       0.016       0.024       0.028

###Transposed

n = 400

                Mean        Std.Dev.    Min         Median      Max
    real        0.225       0.001       0.224       0.224       0.226       
    user        0.221       0.005       0.214       0.224       0.224       
    sys         0.004       0.006       0.000       0.000       0.012       

n = 500

                Mean        Std.Dev.    Min         Median      Max
    real        0.477       0.000       0.477       0.477       0.478       
    user        0.474       0.002       0.472       0.473       0.477       
    sys         0.003       0.002       0.000       0.004       0.004       

n = 630

                Mean        Std.Dev.    Min         Median      Max
    real        0.997       0.001       0.996       0.997       0.998       
    user        0.990       0.004       0.985       0.992       0.994       
    sys         0.007       0.004       0.004       0.004       0.012       

n = 800

                Mean        Std.Dev.    Min         Median      Max
    real        2.162       0.016       2.145       2.157       2.183       
    user        2.154       0.017       2.141       2.144       2.178       
    sys         0.007       0.004       0.004       0.004       0.012       

n = 1000

                Mean        Std.Dev.    Min         Median      Max
    real        4.335       0.000       4.334       4.335       4.335       
    user        4.321       0.002       4.318       4.322       4.323       
    sys         0.013       0.002       0.012       0.012       0.016       

n = 1800

                Mean        Std.Dev.    Min         Median      Max
    real        27.601      0.052       27.541      27.594      27.669      
    user        27.566      0.057       27.508      27.547      27.643      
    sys         0.031       0.005       0.024       0.032       0.036 

###OpenMP

n = 400

                Mean        Std.Dev.    Min         Median      Max
    real        0.013       0.000       0.013       0.013       0.013       
    user        0.077       0.002       0.075       0.077       0.081       
    sys         0.004       0.003       0.000       0.005       0.007       

n = 500

                Mean        Std.Dev.    Min         Median      Max
    real        0.023       0.001       0.023       0.023       0.024       
    user        0.155       0.006       0.149       0.153       0.162       
    sys         0.001       0.002       0.000       0.000       0.004       

n = 630

                Mean        Std.Dev.    Min         Median      Max
    real        0.057       0.020       0.042       0.043       0.085       
    user        0.340       0.062       0.296       0.296       0.428       
    sys         0.001       0.002       0.000       0.000       0.004       

n = 800

                Mean        Std.Dev.    Min         Median      Max
    real        0.107       0.030       0.086       0.086       0.150       
    user        0.668       0.071       0.614       0.623       0.768       
    sys         0.008       0.003       0.004       0.008       0.012       

n = 1000

                Mean        Std.Dev.    Min         Median      Max
    real        0.254       0.063       0.185       0.240       0.337       
    user        1.541       0.158       1.387       1.479       1.759       
    sys         0.018       0.018       0.004       0.008       0.043       

n = 1800

                Mean        Std.Dev.    Min         Median      Max
    real        5.140       0.160       4.917       5.213       5.289       
    user        37.750      1.618       35.649      38.017      39.585      
    sys         0.069       0.014       0.056       0.064       0.088   