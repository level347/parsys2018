#include <vector>
#include <cstdlib>
#include <cmath>

using Matrix = std::vector<std::vector<double>>;

// initializes a square identity matrix of size n x n
Matrix id(unsigned n) {
    Matrix res;
    res.resize(n);
    for(unsigned i=0; i<n; i++) {
        res[i].resize(n);
        for(unsigned j=0; j<n; j++) {
            res[i][j] = (i == j) ? 1 : 0;
        }
    }
    return res;
}

// initializes a square zero matrix of size n x n
Matrix zero(unsigned n) {
    Matrix res;
    res.resize(n);
    for(unsigned i=0; i<n; i++) {
        res[i].resize(n);
        for(unsigned j=0; j<n; j++) {
            res[i][j] = 0;
        }
    }
    return res;
}

Matrix transpose(const Matrix& m) {
    Matrix res;
    unsigned n = m.size();
    res.resize(n);
    for(unsigned i=0; i<n; i++) {
        res[i].resize(n);
        for(unsigned j=0; j<n; j++) {
            res[i][j] = m[j][i];
        }
    }
    return res;
}

// computes the product of two matrices
Matrix operator*(const Matrix& a, const Matrix& b) {
    unsigned n = a.size();
    unsigned blockSize = sqrt(n);
    unsigned sum = 0;
    Matrix c = zero(n);
    Matrix bTransposed = transpose(b);

    for(unsigned i=0; i<n; i+=blockSize) {
        for(unsigned j=0; j<n; j+=blockSize) {
            for(unsigned k=0; k<n; k+=blockSize) {

                for (unsigned blockI = i; blockI < std::min(i + blockSize, n); blockI++) {
                    for (unsigned blockJ = j; blockJ < std::min(j + blockSize, n); blockJ++) {
                        sum = 0;

                        for (unsigned blockK = k; blockK < std::min(k + blockSize, n); blockK++) {
                            sum += a[blockI][blockK] * bTransposed[blockJ][blockK];
                        }
                        c[blockI][blockJ] += sum;
                    }
                }
            }
        }
    }
    return c;
}


int main(int argc, char** argv) {

    if(argc!=2) return EXIT_FAILURE;
    unsigned n = atoi(argv[1]); // NOLINT(cert-err34-c)
    if(n==0) return EXIT_FAILURE;

    // create two matrices
    auto a = id(n);
    a[0][0] = 42;
    auto b = id(n);

    // compute the product
    auto c = a * b;

    // check that the result is correct
    return (c == a) ? EXIT_SUCCESS : EXIT_FAILURE;
}