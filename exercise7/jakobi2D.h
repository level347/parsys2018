//
// Created by bob on 03.12.18.
//

#ifndef EXERCISE7_JAKOBI2D_H
#define EXERCISE7_JAKOBI2D_H

#include "matrix.h"
#include "stencil.h"

template <int Points>
double sumStencil2D(const Matrix2D<double>& mat, const int i, const int j, const Stencil2D<Points>& stencil) {
    double sum = 0;
    for(int p = 0; p < Points; p++) {
        auto x = stencil.x[p];
        auto y = stencil.y[p];
        sum += mat[i+y][j+x];
    }
    return sum / Points;
}

template <int Points>
Matrix2D<double> jakobi2DSerial(const Matrix2D<double>& mat, const Stencil2D<Points> stencil, const Border2D<double>& border, const double epsilon) {
    auto height = mat.size();
    auto width = mat[0].size();

    int8_t minx = 0;
    int8_t maxx = 0;
    int8_t miny = 0;
    int8_t maxy = 0;

    for(int i = 0; i < Points; i++) {
        auto x = stencil.x[i];
        auto y = stencil.y[i];
        minx = minx > x ? x : minx;
        miny = miny > y ? y : miny;
        maxx = maxx < x ? x : maxx;
        maxy = maxy < y ? y : maxy;
    }

    auto newWidth = width + (maxx - minx);
    auto newHeight = height + (maxy - miny);

    auto borderedMat = createMatrix<double>(newWidth, newHeight);
    auto borderedMat2 = createMatrix<double>(newWidth, newHeight);

    for(int i = 0; i < newHeight; i++) {
        for(int j = 0; j < newWidth; j++) {
            if(j < abs(minx)) {
                borderedMat[i][j] = border.west;
                borderedMat2[i][j] = border.west;
            } else if(j >= width + abs(minx)) {
                borderedMat[i][j] = border.east;
                borderedMat2[i][j] = border.east;
            } else if(i < abs(miny)) {
                borderedMat[i][j] = border.north;
                borderedMat2[i][j] = border.north;
            } else if(i >= height + abs(miny)) {
                borderedMat[i][j] = border.south;
                borderedMat2[i][j] = border.south;
            } else {
                borderedMat[i][j] = mat[i-abs(miny)][j-abs(minx)];
                borderedMat2[i][j] = mat[i-abs(miny)][j-abs(minx)];
            }
        }
    }

    double changes;
    Matrix2D<double>* prevMat = &borderedMat;
    Matrix2D<double>* nextMat = &borderedMat2;

    int k = 0;
    do {
        changes = 0.0;

        for(int i = abs(miny); i < newHeight - maxy; i++) {
            for(int j = abs(minx); j < newWidth - maxx; j++) {
                (*nextMat)[i][j] = sumStencil2D<Points>(*prevMat, i, j, stencil);
                changes += std::abs((*nextMat)[i][j] - (*prevMat)[i][j]);
            }
        }

        auto temp = prevMat;
        prevMat = nextMat;
        nextMat = temp;
        k++;

    } while(changes > epsilon);


    auto retMat = createMatrix<double>(width, height);

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            auto y = i + abs(miny);
            auto x = j + abs(minx);
            retMat[i][j] = (*prevMat)[y][x];
        }
    }
    return retMat;
}

template<int Points>
Matrix2D<double> jakobi2DParallel(const Matrix2D<double>& mat, const Stencil2D<Points> stencil, const Border2D<double>& border, const double epsilon) {
    auto height = mat.size();
    auto width = mat[0].size();

    int8_t minx = 0;
    int8_t maxx = 0;
    int8_t miny = 0;
    int8_t maxy = 0;

    for(int i = 0; i < Points; i++) {
        auto x = stencil.x[i];
        auto y = stencil.y[i];
        minx = minx > x ? x : minx;
        miny = miny > y ? y : miny;
        maxx = maxx < x ? x : maxx;
        maxy = maxy < y ? y : maxy;
    }

    auto newWidth = width + (maxx - minx);
    auto newHeight = height + (maxy - miny);

    auto borderedMat = createMatrix<double>(newWidth, newHeight);
    auto borderedMat2 = createMatrix<double>(newWidth, newHeight);
    for(int i = 0; i < newHeight; i++) {
        for(int j = 0; j < newWidth; j++) {
            if(j < abs(minx)) {
                borderedMat[i][j] = border.west;
                borderedMat2[i][j] = border.west;
            } else if(j >= width + abs(minx)) {
                borderedMat[i][j] = border.east;
                borderedMat2[i][j] = border.east;
            } else if(i < abs(miny)) {
                borderedMat[i][j] = border.north;
                borderedMat2[i][j] = border.north;
            } else if(i >= height + abs(miny)) {
                borderedMat[i][j] = border.south;
                borderedMat2[i][j] = border.south;
            } else {
                borderedMat[i][j] = mat[i-abs(miny)][j-abs(minx)];
                borderedMat2[i][j] = mat[i-abs(miny)][j-abs(minx)];
            }
        }
    }


    double changes = 0.0;
    Matrix2D<double>* prevMat = &borderedMat;
    Matrix2D<double>* nextMat = &borderedMat2;

    do {
            changes = 0.0;
            #pragma omp parallel for reduction(+:changes) schedule(dynamic)
            for (int i = abs(miny); i < newHeight - maxy; i++) {
                for (int j = abs(minx); j < newWidth - maxx; j++) {
                    (*nextMat)[i][j] = sumStencil2D<Points>(*prevMat, i, j, stencil);
                    auto diff = std::abs((*nextMat)[i][j] - (*prevMat)[i][j]);

                    changes += diff;
                }
            }

            auto temp = prevMat;
            prevMat = nextMat;
            nextMat = temp;
    } while (changes > epsilon);

    auto retMat = createMatrix<double>(width, height);

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            auto y = i + abs(miny);
            auto x = j + abs(minx);
            retMat[i][j] = (*prevMat)[y][x];
        }
    }
    return retMat;
}

#endif //EXERCISE7_JAKOBI2D_H
