#include <cmath>
#include "matrix.h"

std::tuple<int,int,int> hsvToRgb(int h, double s, double v) {
    double c = v * s;
    double x = c*(1-abs((h/60)%2-1));
    double m = v-c;
    std::tuple<int,int,int> retval{0,0,0};

    if(h < 60) {
        retval = {c,x,0};
    } else if(h < 120) {
        retval = {x,c,0};
    } else if(h < 180) {
        retval = {0,c,x};
    } else if(h < 240) {
        retval = {0,x,c};
    } else if(h < 300) {
        retval = {x,0,c};
    } else {
        retval = {c,0,x};
    }

    return std::make_tuple(floor((std::get<0>(retval) + m) * 255),floor((std::get<1>(retval) + m) * 255),floor((std::get<2>(retval) + m) * 255));
}