//
// Created by bob on 03.12.18.
//

#ifndef EXERCISE7_MATRIX_H
#define EXERCISE7_MATRIX_H

#include <vector>
#include <iostream>
#include <fstream>
#include <tuple>

std::tuple<int,int,int> hsvToRgb(int h, double s, double v);

template <class T>
using Matrix1D = std::vector<T>;

template <class T>
using Matrix2D = std::vector<std::vector<T>>;

template <class T>
using Matrix3D = std::vector<std::vector<std::vector<T>>>;

template <typename T>
Matrix1D<T> createMatrix(const std::size_t width) {
    Matrix1D<T> matrix(width, 0);
    return matrix;
}

template <typename T>
Matrix2D<T> createMatrix(const std::size_t width, const std::size_t height) {
    Matrix2D<T> matrix(height);
    for(std::size_t i = 0; i < height; i++) {
        matrix[i] = std::vector<T>(width, 0);
    }
    return matrix;
}

template <typename T>
Matrix3D<T> createMatrix(const std::size_t width, const std::size_t height, const std::size_t depth) {
    Matrix3D<T> matrix(height);
    for(std::size_t i = 0; i < height; i++) {
        matrix[i] = std::vector<std::vector<T>>(width);
        for(std::size_t j = 0; j < width; j++) {
            matrix[i][j] = std::vector<T>(depth,0);
        }
    }
    return matrix;
}


template <typename T>
void printMatrix(Matrix2D<T> matrix) {
    for(std::size_t i = 0; i < matrix.size(); i++) {
        auto row = matrix[i];
        for(std::size_t j = 0; j < row.size(); j++) {
            std::cout << row[j] << ";";
        }
        std::cout << std::endl;
    }
}

template <typename T>
void printMatrix(Matrix1D<T> matrix) {
    for(std::size_t i = 0; i < matrix.size(); i++) {
        std::cout << matrix[i] << ";";

    }
    std::cout << std::endl;
}

template <typename T>
void printppm(Matrix1D<T>& mat, std::string filename) {
    std::ofstream ppmFile;
    std::string fileName(filename);
    ppmFile.open(fileName);
    if (!ppmFile.is_open())
    {
        std::cerr << "Could not open: " << fileName << std::endl;
        return;
    }
    auto width = mat.size();

    ppmFile << "P3\n" << width << " " << 1 << "\n" << 255 << "\n";

    double max = 0.0;
    for(int i = 0; i < width; i++) {
        max = max < mat[i] ? mat[i] : max;
    }

    for(int i = 0; i < width; i++) {
        double val = mat[i] / max; // scale to 0.0 - 1.0
            // map values 0.0 - 1.0 to values from 60 - 359 (yellow->green->blue->magenta->red)
        auto h = 60 + static_cast<uint8_t>(val * 299);
        auto rgb = hsvToRgb(h,1.0,1.0);
        ppmFile << std::get<0>(rgb) << " " << std::get<1>(rgb) << " " << std::get<2>(rgb) << "\n";

    }

    ppmFile.close();

}

template <typename T>
void printppm(Matrix2D<T>& mat, std::string filename)
{
    std::ofstream ppmFile;
    std::string fileName(filename);
    ppmFile.open(fileName);
    if (!ppmFile.is_open())
    {
        std::cerr << "Could not open: " << fileName << std::endl;
        return;
    }
    auto height = mat.size();
    auto width = mat[0].size();

    ppmFile << "P3\n" << width << " " << height << "\n" << 255 << "\n";

    double max = 0.0;
    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            max = max < mat[i][j] ? mat[i][j] : max;
        }
    }

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            double val = mat[i][j] / max; // scale to 0.0 - 1.0
            // map values 0.0 - 1.0 to values from 60 - 359 (yellow->green->blue->magenta->red)
            auto h = 60 + static_cast<uint8_t>(val * 299);
            auto rgb = hsvToRgb(h,1.0,1.0);
            ppmFile << std::get<0>(rgb) << " " << std::get<1>(rgb) << " " << std::get<2>(rgb) << "\n";
        }
    }

    ppmFile.close();
}

#endif //EXERCISE7_MATRIX_H
