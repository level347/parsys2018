#include <iostream>
#include <cstdint>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <iterator>

#include "matrix.h"
#include "stencil.h"
#include "jakobi2D.h"
#include "jakobi1D.h"
#include "jakobi3D.h"

typedef bool(*testFunction)();

bool compareFiles(const std::string& p1, const std::string& p2) {
    std::ifstream f1(p1, std::ifstream::binary|std::ifstream::ate);
    std::ifstream f2(p2, std::ifstream::binary|std::ifstream::ate);

    if (f1.fail() || f2.fail()) {
        return false; //file problem
    }

    if (f1.tellg() != f2.tellg()) {
        return false; //size mismatch
    }

    //seek back to beginning and use std::equal to compare contents
    f1.seekg(0, std::ifstream::beg);
    f2.seekg(0, std::ifstream::beg);
    return std::equal(std::istreambuf_iterator<char>(f1.rdbuf()),
                      std::istreambuf_iterator<char>(),
                      std::istreambuf_iterator<char>(f2.rdbuf()));
}

bool stencil1DTest() {

    Matrix1D<double> mat = createMatrix<double>(100);
    Stencil1D<3> stencil= {
            {1, -1}, // x values
    };

    Border1D<double> border {
            0.0, 1.0 // east, west
    };

    double epsilon = 0.01;

    Matrix1D<double> retmat = jakobi1DSerial<3>(mat, stencil, border, epsilon);

    printppm(retmat, "unittest1DSerial.ppm");
    return !compareFiles("unittest1DSerial.ppm", "../tests/unittest1DSerial.ppm");
}

bool stencil1DTestParallel() {

    Matrix1D<double> mat = createMatrix<double>(100);
    Stencil1D<3> stencil= {
            {1, -1}, // x values
    };

    Border1D<double> border {
            0.0, 1.0 // east, west
    };

    double epsilon = 0.01;

    Matrix1D<double> retmat = jakobi1DParallel<3>(mat, stencil, border, epsilon);

    printppm(retmat, "unittest1DParallel.ppm");
    return !compareFiles("unittest1DParallel.ppm", "../tests/unittest1DSerial.ppm");
}

bool stencil2DTest() {

    Matrix2D<double> mat = createMatrix<double>(100, 100);
    Stencil2D<4> stencil= {
            {0, 1, -1, 0}, // x values
            {1, 0, 0, -1} // y values
    };

    Border2D<double> border {
            0.0, 1.0, 0.0, 1.0 // north, east, south, west
    };

    double epsilon = 2.0;

    auto retmat = jakobi2DSerial<4>(mat, stencil, border, epsilon);
    printppm(retmat, "unittest2DSerial.ppm");
    return !compareFiles("unittest2DSerial.ppm", "../tests/unittest2DSerial.ppm");
}

bool stencil2DTestParallel() {

    Matrix2D<double> mat = createMatrix<double>(100, 100);
    Stencil2D<4> stencil= {
            {0, 1, -1, 0}, // x values
            {1, 0, 0, -1} // y values
    };

    Border2D<double> border {
            0.0, 1.0, 0.0, 1.0 // north, east, south, west
    };

    double epsilon = 2.0;

    auto retmat = jakobi2DParallel<4>(mat, stencil, border, epsilon);
    printppm(retmat, "unittest2DParallel.ppm");
    return !compareFiles("unittest2DParallel.ppm", "../tests/unittest2DSerial.ppm");
}

bool stencil3DTest() {

    Matrix3D<double> mat = createMatrix<double>(30, 30, 30);
    Stencil3D<6> stencil= {
            {0, 0, -1, 1, 0,0}, // x values
            {0, 0, 0, 0,-1, 1}, // y values
            {-1,1, 0, 0,0,0} // z values
    };

    Border3D<double> border {
            0.0, 1.0, 0.0, 1.0, 0.0, 0.0 // north, east, south, west
    };

    double epsilon = 2.0;

    auto retmat = jakobi3DSerial<6>(mat, stencil, border, epsilon);
    printppm(retmat[15], "unittest3DSerial.ppm");
    return !compareFiles("unittest3DSerial.ppm", "../tests/unittest3DSerial.ppm");
}

bool stencil3DTestParallel() {

    Matrix3D<double> mat = createMatrix<double>(30, 30, 30);
    Stencil3D<6> stencil= {
            {0, 0, -1, 1, 0,0}, // x values
            {0, 0, 0, 0,-1, 1}, // y values
            {-1,1, 0, 0,0,0} // z values
    };

    Border3D<double> border {
            0.0, 1.0, 0.0, 1.0, 0.0, 0.0 // north, east, south, west
    };

    double epsilon = 2.0;

    auto retmat = jakobi3DParallel<6>(mat, stencil, border, epsilon);
    printppm(retmat[15], "unittest3DParallel.ppm");
    return !compareFiles("unittest3DParallel.ppm", "../tests/unittest3DSerial.ppm");
}

std::pair<testFunction, std::string> tests[] = {{stencil3DTest, "Simple 3D stencil"},
                                                {stencil2DTest, "Simple 2D stencil"},
                                                {stencil1DTest, "Simple 1D stencil"},
                                                {stencil1DTestParallel, "Simple 1D stencil parallel"},
                                                {stencil2DTestParallel, "Simple 2D stencil parallel"},
                                                {stencil3DTestParallel, "Simple 3D stencil parallel"},};

int main(void) {
   int n = sizeof(tests)/sizeof(tests[0]);
    int failedTests = 0;
    for(int idx = 0; idx < n; idx++) {
        bool result = 0;
        try {
            result = tests[idx].first();
        }
        catch (std::exception e) {
            result = 1;
        }
        if(result) {
            std::cout << "\033[1;31mTest failed: " << tests[idx].second << "\033[0m" << std::endl;
            failedTests++;
        }
    }
    if(failedTests == 0) {
        std::cout << "\033[1;32m" << n-failedTests << " of " << n << " tests completed successfully" << "\033[0m" << std::endl;
    }
    else {
        std::cout << n-failedTests << " of " << n << " tests completed successfully" << std::endl;
    }

}