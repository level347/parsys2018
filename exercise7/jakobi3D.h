//
// Created by bob on 04.12.18.
//

#ifndef EXERCISE7_JAKOBI3D_H
#define EXERCISE7_JAKOBI3D_H

#include "matrix.h"
#include "stencil.h"

template <int Points>
double sumStencil3D(const Matrix3D<double>& mat, const int i, const int j, const int k, const Stencil3D<Points>& stencil) {
    double sum = 0;
    for(int p = 0; p < Points; p++) {
        auto x = stencil.x[p];
        auto y = stencil.y[p];
        auto z = stencil.z[p];
        sum += mat[i+y][j+x][k+z];
    }
    return sum / Points;
}

template <int Points>
Matrix3D<double> jakobi3DSerial(const Matrix3D<double>& mat, const Stencil3D<Points> stencil, const Border3D<double>& border, const double epsilon) {
    auto height = mat.size();
    auto width = mat[0].size();
    auto depth = mat[0][0].size();

    int8_t minx = 0;
    int8_t maxx = 0;
    int8_t miny = 0;
    int8_t maxy = 0;
    int8_t minz = 0;
    int8_t maxz = 0;

    for(int i = 0; i < Points; i++) {
        auto x = stencil.x[i];
        auto y = stencil.y[i];
        auto z = stencil.z[i];
        minx = minx > x ? x : minx;
        miny = miny > y ? y : miny;
        minz = minz > z ? z : minz;
        maxx = maxx < x ? x : maxx;
        maxy = maxy < y ? y : maxy;
        maxz = maxz < z ? z : maxz;
    }

    auto newWidth = width + (maxx - minx);
    auto newHeight = height + (maxy - miny);
    auto newDepth = depth + (maxz - minz);

    auto borderedMat = createMatrix<double>(newWidth, newHeight, newDepth);
    auto borderedMat2 = createMatrix<double>(newWidth, newHeight, newDepth);

    for(int i = 0; i < newHeight; i++) {
        for(int j = 0; j < newWidth; j++) {
            for(int k = 0; k < newDepth; k++) {
                if (j < abs(minx)) {
                    borderedMat[i][j][k] = border.west;
                    borderedMat2[i][j][k] = border.west;
                } else if (j >= width + abs(minx)) {
                    borderedMat[i][j][k] = border.east;
                    borderedMat2[i][j][k] = border.east;
                } else if (i < abs(miny)) {
                    borderedMat[i][j][k] = border.north;
                    borderedMat2[i][j][k] = border.north;
                } else if (i >= height + abs(miny)) {
                    borderedMat[i][j][k] = border.south;
                    borderedMat2[i][j][k] = border.south;
                } else if (k < abs(minz)) {
                    borderedMat[i][j][k] = border.front;
                    borderedMat2[i][j][k] = border.front;
                } else if (k >= depth + abs(minz)) {
                    borderedMat[i][j][k] = border.back;
                    borderedMat2[i][j][k] = border.back;
                } else {
                    borderedMat[i][j][k] = mat[i - abs(miny)][j - abs(minx)][k - abs(minz)];
                    borderedMat2[i][j][k] = mat[i - abs(miny)][j - abs(minx)][k - abs(minz)];
                }
            }
        }
    }

    double changes;
    Matrix3D<double>* prevMat = &borderedMat;
    Matrix3D<double>* nextMat = &borderedMat2;

    int k = 0;
    do {
        changes = 0.0;

        for(int i = abs(miny); i < newHeight - maxy; i++) {
            for(int j = abs(minx); j < newWidth - maxx; j++) {
                for(int k = abs(minz); k < newDepth - maxz; k++) {
                    (*nextMat)[i][j][k] = sumStencil3D<Points>(*prevMat, i, j, k, stencil);
                    changes += std::abs((*nextMat)[i][j][k] - (*prevMat)[i][j][k]);
                }
            }
        }

        auto temp = prevMat;
        prevMat = nextMat;
        nextMat = temp;
        k++;

    } while(changes > epsilon);


    auto retMat = createMatrix<double>(width, height, depth);

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            for(int k = 0; k < depth; k++) {
                auto y = i + abs(miny);
                auto x = j + abs(minx);
                auto z = k + abs(minz);
                retMat[i][j][k] = (*prevMat)[y][x][z];
            }
        }
    }
    return retMat;
}

template <int Points>
Matrix3D<double> jakobi3DParallel(const Matrix3D<double>& mat, const Stencil3D<Points> stencil, const Border3D<double>& border, const double epsilon) {
    auto height = mat.size();
    auto width = mat[0].size();
    auto depth = mat[0][0].size();

    int8_t minx = 0;
    int8_t maxx = 0;
    int8_t miny = 0;
    int8_t maxy = 0;
    int8_t minz = 0;
    int8_t maxz = 0;

    for(int i = 0; i < Points; i++) {
        auto x = stencil.x[i];
        auto y = stencil.y[i];
        auto z = stencil.z[i];
        minx = minx > x ? x : minx;
        miny = miny > y ? y : miny;
        minz = minz > z ? z : minz;
        maxx = maxx < x ? x : maxx;
        maxy = maxy < y ? y : maxy;
        maxz = maxz < z ? z : maxz;
    }

    auto newWidth = width + (maxx - minx);
    auto newHeight = height + (maxy - miny);
    auto newDepth = depth + (maxz - minz);

    auto borderedMat = createMatrix<double>(newWidth, newHeight, newDepth);
    auto borderedMat2 = createMatrix<double>(newWidth, newHeight, newDepth);

    for(int i = 0; i < newHeight; i++) {
        for(int j = 0; j < newWidth; j++) {
            for(int k = 0; k < newDepth; k++) {
                if (j < abs(minx)) {
                    borderedMat[i][j][k] = border.west;
                    borderedMat2[i][j][k] = border.west;
                } else if (j >= width + abs(minx)) {
                    borderedMat[i][j][k] = border.east;
                    borderedMat2[i][j][k] = border.east;
                } else if (i < abs(miny)) {
                    borderedMat[i][j][k] = border.north;
                    borderedMat2[i][j][k] = border.north;
                } else if (i >= height + abs(miny)) {
                    borderedMat[i][j][k] = border.south;
                    borderedMat2[i][j][k] = border.south;
                } else if (k < abs(minz)) {
                    borderedMat[i][j][k] = border.front;
                    borderedMat2[i][j][k] = border.front;
                } else if (k >= depth + abs(minz)) {
                    borderedMat[i][j][k] = border.back;
                    borderedMat2[i][j][k] = border.back;
                } else {
                    borderedMat[i][j][k] = mat[i - abs(miny)][j - abs(minx)][k - abs(minz)];
                    borderedMat2[i][j][k] = mat[i - abs(miny)][j - abs(minx)][k - abs(minz)];
                }
            }
        }
    }

    double changes;
    Matrix3D<double>* prevMat = &borderedMat;
    Matrix3D<double>* nextMat = &borderedMat2;

    int k = 0;
    do {
        changes = 0.0;

#pragma omp parallel for schedule(dynamic) reduction(+:changes)
        for(int i = abs(miny); i < newHeight - maxy; i++) {
            for(int j = abs(minx); j < newWidth - maxx; j++) {
                for(int k = abs(minz); k < newDepth - maxz; k++) {
                    (*nextMat)[i][j][k] = sumStencil3D<Points>(*prevMat, i, j, k, stencil);
                    changes += std::abs((*nextMat)[i][j][k] - (*prevMat)[i][j][k]);
                }
            }
        }

        auto temp = prevMat;
        prevMat = nextMat;
        nextMat = temp;
        k++;

    } while(changes > epsilon);


    auto retMat = createMatrix<double>(width, height, depth);

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            for(int k = 0; k < depth; k++) {
                auto y = i + abs(miny);
                auto x = j + abs(minx);
                auto z = k + abs(minz);
                retMat[i][j][k] = (*prevMat)[y][x][z];
            }
        }
    }
    return retMat;
}

#endif //EXERCISE7_JAKOBI3D_H
