//
// Created by bob on 03.12.18.
//

#ifndef EXERCISE7_STENCIL_H
#define EXERCISE7_STENCIL_H

#include <array>

template <std::size_t Points>
struct Stencil1D {
    int8_t x[Points];
};

template <std::size_t Points>
struct Stencil2D {
    int8_t x[Points];
    int8_t y[Points];
};

template <std::size_t Points>
struct Stencil3D {
    int8_t x[Points];
    int8_t y[Points];
    int8_t z[Points];
};

template<typename T>
struct Border1D {
    T east ;
    T west;
};

template<typename T>
struct Border2D {
    T north;
    T east ;
    T south;
    T west;
};

template<typename T>
struct Border3D {
    T north;
    T east ;
    T south;
    T west;
    T front;
    T back;
};

#endif //EXERCISE7_STENCIL_H
