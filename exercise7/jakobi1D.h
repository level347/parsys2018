//
// Created by bob on 04.12.18.
//

#ifndef EXERCISE7_JAKOBI1D_H
#define EXERCISE7_JAKOBI1D_H

#include "matrix.h"
#include "stencil.h"

template <int Points>
double sumStencil1D(const Matrix1D<double>& mat, const int i, const Stencil1D<Points>& stencil) {
    double sum = 0.0;
    for(int p = 0; p < Points; p++) {
        auto x = stencil.x[p];
        sum += mat[i+x];
    }
    return sum / Points;
}

template <int Points>
Matrix1D<double> jakobi1DSerial(const Matrix1D<double>& mat, const Stencil1D<Points> stencil, const Border1D<double>& border, const double epsilon) {
    auto width = mat.size();

    int8_t minx = 0;
    int8_t maxx = 0;

    for(int i = 0; i < Points; i++) {
        auto x = stencil.x[i];
        minx = minx > x ? x : minx;
        maxx = maxx < x ? x : maxx;
    }

    auto newWidth = width + (maxx - minx);

    auto borderedMat = createMatrix<double>(newWidth);
    auto borderedMat2 = createMatrix<double>(newWidth);

    for(int i = 0; i < newWidth; i++) {
        if(i < abs(minx)) {
            borderedMat[i] = border.west;
            borderedMat2[i] = border.west;
        } else if(i >= width + abs(minx)) {
            borderedMat[i] = border.east;
            borderedMat2[i] = border.east;
        } else {
            borderedMat[i] = mat[i-abs(minx)];
            borderedMat2[i] = mat[i-abs(minx)];
        }
    }

    double changes;
    Matrix1D<double>* prevMat = &borderedMat;
    Matrix1D<double>* nextMat = &borderedMat2;

    int k = 0;
    do {
        changes = 0.0;

        for(int i = abs(minx); i < newWidth - maxx; i++) {
            (*nextMat)[i] = sumStencil1D<Points>(*prevMat, i, stencil);
            changes += std::abs((*nextMat)[i] - (*prevMat)[i]);
        }


        auto temp = prevMat;
        prevMat = nextMat;
        nextMat = temp;
        k++;

    } while(changes > epsilon);


    auto retMat = createMatrix<double>(width);

    for(int i = 0; i < width; i++) {
        auto x = i + abs(minx);
        retMat[i] = (*prevMat)[x];
    }
    return retMat;
}

template <int Points>
Matrix1D<double> jakobi1DParallel(const Matrix1D<double>& mat, const Stencil1D<Points> stencil, const Border1D<double>& border, const double epsilon) {
    auto width = mat.size();

    int8_t minx = 0;
    int8_t maxx = 0;

    for(int i = 0; i < Points; i++) {
        auto x = stencil.x[i];
        minx = minx > x ? x : minx;
        maxx = maxx < x ? x : maxx;
    }

    auto newWidth = width + (maxx - minx);

    auto borderedMat = createMatrix<double>(newWidth);
    auto borderedMat2 = createMatrix<double>(newWidth);

    for(int i = 0; i < newWidth; i++) {
        if(i < abs(minx)) {
            borderedMat[i] = border.west;
            borderedMat2[i] = border.west;
        } else if(i >= width + abs(minx)) {
            borderedMat[i] = border.east;
            borderedMat2[i] = border.east;
        } else {
            borderedMat[i] = mat[i-abs(minx)];
            borderedMat2[i] = mat[i-abs(minx)];
        }
    }

    double changes;
    Matrix1D<double>* prevMat = &borderedMat;
    Matrix1D<double>* nextMat = &borderedMat2;

    int k = 0;
    do {
        changes = 0.0;

#pragma omp parallel for schedule(dynamic) reduction(+:changes)
        for(int i = abs(minx); i < newWidth - maxx; i++) {
            (*nextMat)[i] = sumStencil1D<Points>(*prevMat, i, stencil);
            changes += std::abs((*nextMat)[i] - (*prevMat)[i]);
        }


        auto temp = prevMat;
        prevMat = nextMat;
        nextMat = temp;
        k++;

    } while(changes > epsilon);


    auto retMat = createMatrix<double>(width);

    for(int i = 0; i < width; i++) {
        auto x = i + abs(minx);
        retMat[i] = (*prevMat)[x];
    }
    return retMat;
}

#endif //EXERCISE7_JAKOBI1D_H
