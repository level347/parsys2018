#Exercise 7


##Benchmark Setup

We used a repetition count of 5 and used the median value as result. The Chronotimer was used for execution time
measurements. The benchmarks were performed on LCC2 with 1,2,4 and 8 threads as well as the serial implementation.
The given benchmark criteria were used:

    * 2D grid of 512x512 cells
    * 5 point heat stencil
    * double datatype
    * Iterate until Epsilon (as the sum of all changes across the grid) is less than 10.0
    * North/East/South/West boundaries fixed at 1.0, 0.5, 0.0 and -0.5 respectively
    * All initial values should be set to 0.0
    * Use the -O3 and -march=native compiler flags (not -Ofast)

 
##Benchmark Results

|            | 512x512 |
|------------|---------|
| Serial     | 618     |
| Parallel 1 | 1827    |
| Parallel 2 | 980     |
| Parallel 4 | 878     |
| Parallel 8 | 600     |
| Speedup    | 103%    |

##Benchmark Discussion

The results of the benchmark show that the thread creation overhead of the OpenMP parallel for statement is too large
to get significant speedup for the given problem size. On our local desktop environment with a i7-4770k processor the
speedup is about 2. We think it is necessary to parallelize over multiple iteration steps to get further speedup for this
problem sizes. However, this can also lead to increased memory consumption and in consequence to more cache misses.