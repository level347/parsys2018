#include <omp.h>

#include "matrix.h"
#include "stencil.h"
#include "jakobi2D.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    if(argc < 2) {
        return EXIT_FAILURE;
    }
    const auto size = static_cast<const size_t>(atoi(argv[1]));
    int num_threads = -1;
    if(argc >= 3) {
        num_threads = atoi(argv[2]);
    }
    Matrix2D<double> retmat;


    Matrix2D<double> mat = createMatrix<double>(size, size);
    Stencil2D<5> stencil= {
        {0, 0, 0, 1, -1,}, // x values
        {-1, 0, 1, 0, 0,} // y values
    };

    Border2D<double> border {
        1.0, 0.5, 0.0, -0.5 // north, east, south, west
    };

    double epsilon = 10.0;

    if(num_threads < 0) {
        {
            ChronoTimer timer("Serial Stencil Benchmark");
            retmat = jakobi2DSerial<5>(mat, stencil, border, epsilon);
        }
      //  printppm(retmat, "serial.ppm");
    }
    else {
        if (num_threads != 0) {
            omp_set_num_threads(num_threads);
        }
        {
            ChronoTimer timer("Parallel Stencil Benchmark");
            retmat = jakobi2DParallel<5>(mat, stencil, border, epsilon);
        }
      //  printppm(retmat, "parallel.ppm");
    }
}
