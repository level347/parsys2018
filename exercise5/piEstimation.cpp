
#include <random>
#include <omp.h>
#include <iostream>
#include <bitset>

#include "fastrand.h"

constexpr int vecSize=4;

double estimatePiParallel(int samples, int seed) {
    int inside = 0;
    int sampleCount = (samples/vecSize)*vecSize + vecSize;
    #pragma omp parallel shared(inside)
    {
        uint32_t privSeed = seed+omp_get_thread_num()*8;
        uint32_t seedsA[] = {privSeed, privSeed+1, privSeed+2,privSeed+3};
        uint32_t seedsB[] = {privSeed+4, privSeed+5, privSeed+6,privSeed+7};
        FastRandVec fastRandVecA(seedsA);
        FastRandVec fastRandVecB(seedsB);

        #pragma omp for reduction(+:inside)
        for(int idx = 0; idx < sampleCount; idx+=vecSize){
            float x[vecSize];
            float y[vecSize];

            uint32_t* xi = fastRandVecA.generate();
            uint32_t* yi = fastRandVecB.generate();

            #pragma omp simd
            for(int jdx = 0; jdx < vecSize; jdx++) {
                x[jdx] = (float)xi[jdx]/(float)UINT32_MAX;
                y[jdx] = (float)yi[jdx]/(float)UINT32_MAX;
                inside += (x[jdx]*x[jdx]+y[jdx]*y[jdx] <= 1.0);
            }
        }
    };

    return (float)(inside*4)/(float)sampleCount;
}


double estimatePiSerial(int samples, int seed) {
    FastRand fastRand(seed);

    int inside = 0;
    for(int idx = 0; idx < samples; idx++){
        float x = (float)fastRand.generate()/(float)UINT32_MAX;
        float y =  (float)fastRand.generate()/(float)UINT32_MAX;
        inside += (x*x+y*y <= 1.0);
    }
    return (float)(inside*4)/(float)samples;
}
