
#include <omp.h>
#include "nQueens.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    if(argc < 2) {
        return EXIT_FAILURE;
    }
    int n = atoi(argv[1]);
    int num_threads = -1;
    if(argc >= 3) {
        num_threads = atoi(argv[2]);
    }
    uint64_t solution = 0;
    if(num_threads < 0) {
        {
            ChronoTimer timer("Serial N-Queens");
            solution = nQueensSerial(n);
        }
        std::cout << "Solution: " << solution << std::endl;
    }
    else {
        if (num_threads != 0) {
            omp_set_num_threads(num_threads);
        }
        {
            ChronoTimer timer("Parallel N-Queens");
            solution = nQueensParallel(n);
        }
        std::cout << "Solution: " << solution << std::endl;
    }
}

