#include <iostream>
#include <cstdint>
#include <cmath>
#include <algorithm>

#include "mergeSort.h"
#include "piEstimation.h"
#include "nQueens.h"

typedef bool(*testFunction)();

bool mergeSortRandomArray() {
    int size = 100;
    VALUE* array = new VALUE[size];
    fillRandomized(array, size);
    mergeSortSerial(array, size);
    return (!isSorted(array,size));
}
bool mergeSortRandomArrayPar() {
    int size = 100;
    VALUE* array = new VALUE[size];
    fillRandomized(array, size);
    mergeSortParallel(array, size);
    return (!isSorted(array,size));
}

bool piEstimation() {
    double pi = estimatePiSerial(10000, 107);
    double eps = 0.01;
    return (abs(pi - 3.1292) > eps);
}

bool piEstimationPar() {
    double pi = estimatePiParallel(10000, 107);
    double eps = 0.01;
    return (abs(pi - 3.1464) > eps);
}

uint64_t nQueensSolution[] = {0,1,0,0,2,10,4,40,92,352,724,2680,14200,73712,365596,2279184,14772512,95815104,666090624,
                              4968057848,39029188884,314666222712,2691008701644};
constexpr int maxSolutions = sizeof(nQueensSolution)/sizeof(nQueensSolution[0]);
constexpr int maxTestSolutions = 10;

bool nQueens() {
    for(int i = 0; i < std::min(maxTestSolutions,maxSolutions); i++) {
        if(nQueensSerial(i) != nQueensSolution[i]) {
            return true;
        }
    }
    return false;
}

bool nQueensPar() {
    for(int i = 0; i < std::min(maxTestSolutions,maxSolutions); i++) {
        if(nQueensParallel(i) != nQueensSolution[i]) {
            return true;
        }
    }
    return false;
}

std::pair<testFunction, std::string> tests[] = {{mergeSortRandomArray, "mergeSortRandomArray"},
                                                {mergeSortRandomArrayPar, "mergeSortRandomArrayParallel"},
                                                {piEstimation, "piEstimation"},
                                                {piEstimationPar, "piEstimationParallel"},
                                                {nQueens, "nQueens"},
                                                {nQueensPar, "nQueensParallel"}};

int main(void) {
   int n = sizeof(tests)/sizeof(tests[0]);
    int failedTests = 0;
    for(int idx = 0; idx < n; idx++) {
        bool result;
        try {
            result = tests[idx].first();
        }
        catch (std::exception e) {
            result = 1;
        }
        if(result) {
            std::cout << "\033[1;31mTest failed: " << tests[idx].second << "\033[0m" << std::endl;
            failedTests++;
        }
    }
    if(failedTests == 0) {
        std::cout << "\033[1;32m" << n-failedTests << " of " << n << " tests completed successfully" << "\033[0m" << std::endl;
    }
    else {
        std::cout << n-failedTests << " of " << n << " tests completed successfully" << std::endl;
    }

}