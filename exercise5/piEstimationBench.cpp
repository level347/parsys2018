
#include <omp.h>

#include "piEstimation.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    if(argc < 2) {
        return EXIT_FAILURE;
    }
    int samples = atoi(argv[1]);
    int num_threads = -1;
    if(argc >= 3) {
        num_threads = atoi(argv[2]);
    }

    double piEstimation = 0.0;
    std::cout.precision(17);
    if(num_threads < 0) {
        {
            ChronoTimer timer("Serial Pi Estimation");
            piEstimation = estimatePiSerial(samples, 107);
        }
        std::cout << "Estimated Pi value: " << piEstimation << std::endl;
    }
    else {
        if (num_threads != 0) {
            omp_set_num_threads(num_threads);
        }
        {
            ChronoTimer timer("Parallel Pi Estimation");
            piEstimation = estimatePiParallel(samples, 107);
        }
        std::cout << "Estimated Pi value: " << piEstimation << std::endl;
    }
}

