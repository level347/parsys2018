#ifndef EXERCISE4_PIESTIMATION_H
#define EXERCISE4_PIESTIMATION_H

double estimatePiSerial(int samples, int seed);
double estimatePiParallel(int samples, int seed);

#endif //EXERCISE4_PIESTIMATION_H
