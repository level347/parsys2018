
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <atomic>
#include <omp.h>


static constexpr uint8_t maxLength = 32;

struct Positions {
    int8_t posY[maxLength];
    uint8_t length;

    void push_back(int8_t y) {
        posY[length] = y;
        length++;
    }

    void pop() {
        length--;
    }
};


bool isNotConflictingPar(int8_t x, int8_t y, const Positions& positions) {
    for(int idx = 0; idx < positions.length; idx++) {
        if (y == positions.posY[idx]) {
            return false;
        }
        if(abs(y - positions.posY[idx]) == (x - idx)) {
            return false;
        }
    }
    return true;
}

bool isNotConflictingSerial(int8_t x, int8_t y, const Positions& positions) {
    for(int idx = 0; idx < positions.length; idx++) {
        if (y == positions.posY[idx]) {
            return false;
        }
        if(abs(y - positions.posY[idx]) == (x - idx)) {
            return false;
        }
    }
    return true;
}


uint64_t nQueensSerial(int8_t n, int8_t x, Positions& positions) {
    if(x == n) {
        return 1;
    }
    uint64_t solution = 0;
    for(int8_t i = 0; i < n; i++) {
        if(isNotConflictingSerial(x, i, positions)) {
            positions.push_back(i);
            solution += nQueensSerial(n, x+1, positions);
            positions.pop();

        }
    }
    return solution;
}

uint64_t nQueensPar(int8_t n, int8_t x, Positions& positions) {
    if(x == n) {
        return 1;
    }
    uint64_t solution = 0;
    for(int8_t i = 0; i < n; i++) {
        if(isNotConflictingPar(x, i, positions)) {
            positions.push_back(i);
            solution += nQueensPar(n, x+1, positions);
            positions.pop();

        }
    }
    return solution;
}


uint64_t nQueensSerial(int n) {
    if(n == 0) {
        return 0;
    }
    Positions positions = {};
    return nQueensSerial(n, 0, positions);
}

uint64_t nQueensParallel(int n) {
    if(n == 0) {
        return 0;
    }
    else if(n == 1) {
        return 1;
    }
    uint64_t solutions = 0;

#pragma omp parallel for schedule(dynamic), reduction(+:solutions)
   for(int idx = 0; idx < n*n; idx++) {
       Positions positions = {};
       positions.push_back(idx/n);
       if(isNotConflictingPar(1, idx%n, positions)) {
           positions.push_back(idx%n);
           solutions += nQueensPar(n, 2, positions);
       }
   }
    return solutions;
}