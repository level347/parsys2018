#Exercise 5

##Optimization

###MergeSort

We removed the cutoff value and the resulting use of std::sort to gain a pure merge sort implementation.
Although this increased execution time, it was done since otherwise it would not be a pure merge sort.

We improved the serial version by using arithmetic masking instead of conditionals in the merge function.
The previous used if statement had a 50% chance of evaluating to 1, which caused many mispredicts.
Memcpy calls were used at end of the merge function, instead of copying each individual value manually.

The parallel version was optimized by setting the omp_set_nested value to true. This was not done in the 
previous version and limited our speedup to two. We used sections to parallelize the recursive mergeSort calls
since all the work is static and equally distributed.
The merge-function was also parallelized by merging from start to mid and end to mid at the same time, by using
omp sections too.

###PiEstimation

The piEstimation example showed that random number generation was the bottleneck. We searched for an alternative 
way to generate random numbers more quickly and found Intel Fast_rand which uses a linear congruential generator.
We implemented this simple algorithm two times. One sequential version and a vectorized version that uses the omp simd
directive and generates 4 values at once.

Instead of using a std-distribution we used a division by int_max to receive a floating point value from 0.0 to 1.0. We 
omitted the calculation of the square root and directly added the boolean value from the comparison instead of using an
if statement.

In the parallel version, the random number generation and the floating point calculations were vectorized with openmp.


###NQueens

We implemented a pop function to be able to remove queen entries from the queen position array. This was used
to prevent unnecessary copies of said array. The array of x dimensions for queen positions was removed, since the x value
is equal to the index in the array.

We also tried to vectorize the conflict check since it contains a loop and is called in the inner-most nQueens calls.
The loop is vectorize-able, but we were not able to force gcc to perform auto-vectorization due to low loop iteration 
count. Since the loop operates on 8-byte integers, vectorization could lead to huge performance increments on AVX capable
systems.

To improve the scaling with increased processor core count and provide better work load distribution we parallelized
the outer two recursive calls. For an invocation with size n, n^2 parallel iterations are invocated, which are scheduled
dynamically. 

##Results

We made some changes to the benchmarking setup compared to exercise 4. 
- Required problem sizes were added.
- Problem sizes of the PiEstimation example were increased 
- Benchmark repetitions were increased from 3 to 5

All times are specified in ms. The speedup is calculated by dividing the serial
execution time by the 8 thread parallel execution time.


###MergeSort

|         | 2.5M | 5M | 10M | 25M | 100M |
|---------|---|----|----|----|----|
| Serial  | 367  |  771  |  1613  |  4217  |  18309  |
| Par 1   | 369  |  843  |  1669  |  4294  |  18383  |
| Par 2   | 188  |  408  |  852  |  2234  |  9757  |
| Par 4   | 109  |  228  |  480  | 1287   | 5752   |
| Par 8   | 86.4  | 163   | 330   | 857   | 3748   |
| Speedup |  424%  |  473%   |  488%  | 492%  |  488%  |

###PiEstimation

|         | 1M | 10M | 25M | 50M | 100M |
|---------|---|----|----|----|----|
| Serial  | 5.39  |  53.6  |  133  | 267   | 534   |
| Par 1   | 3.76  |  37.3  |  93.1  |  186  | 372   |
| Par 2   | 1.98  |  18.7  |  46.6  |  93.2  | 186   |
| Par 4   | 1.06  |  9.49  |  23.4  |  46.8  |  93.5  |
| Par 8   | 0.75  |  4.96  |  11.9   | 23.6   | 46.9   |
| Speedup* | 718%  |  1080%  | 1117%   |  1131%  |  1138%  |

*PiEstimations scales over 800% because of vectorization.

###NQueens

|         | 8 | 10 | 11 | 12 | 13 | 14 |
|---------|---|----|----|----|----|----|
| Serial  | 0.296  | 6.79   | 35.9   | 206   |  1263  |  8263  |
| Par 1   | 0.318  | 6.91  |  36.7  | 212   |  1302  |  8540  |
| Par 2   | 0.255  | 3.55  |  18.5  | 106   |  651  |  4272  |
| Par 4   | 0.220  | 1.89   |  9.41  | 53.8   |  328  | 2149   |
| Par 8   | 0.337  | 1.17  |  4.94  |  27.2  | 166   | 1087   |
| Speedup | 87%  |  580%  |  726%  |  757%  |  760%  |  760%  |

