#include <iostream>
#include <random>
#include <cstring>
#include <algorithm>
#include <omp.h>

#include "mergeSort.h"

static std::mt19937 gen (123);
static std::uniform_int_distribution<int32_t> distrib;

//constexpr int treshold = 1000;

void fillRandomized(VALUE* array, int size) {

    for(int idx=0; idx<size; idx++) {
        array[idx] = distrib(gen);
    }
}

bool isSorted(VALUE* array, int size) {
    for(int idx=1;  idx<size; idx++) {
        if (array[idx-1]>array[idx]) {
            return false;
        }
    }
    return true;
}

void mergeSerial(VALUE* array, int size, VALUE* tmp) {
    int idx = 0;
    int jdx = size/2;
    int tdx = 0;

    while (idx < size/2 && jdx < size) {
        bool x = (array[idx] < array[jdx]);
        bool y = !x;
        tmp[tdx] = array[idx] * x;
        idx += 1*x;
        tmp[tdx] += array[jdx] * y;
        jdx += 1*y;
        tdx++;
    }

    if(idx < size/2) {
        memcpy(&tmp[tdx], &array[idx], sizeof(VALUE)*(size/2 - idx));
    }

    if(jdx < size) {
        memcpy(&tmp[tdx], &array[jdx], sizeof(VALUE)*(size - jdx));
    }

    memcpy(array, tmp, sizeof(VALUE)*size);

}

void mergeParallelLeft(VALUE* array, int size, VALUE* tmp) {
    int idx = 0;
    int jdx = size/2;
    for(int tdx = 0; tdx < size/2; tdx++) {
        bool x = (array[idx] < array[jdx]);
        bool y = !x;
        tmp[tdx] = array[idx] * x;
        idx += 1*x;
        tmp[tdx] += array[jdx] * y;
        jdx += 1*y;
    }
}

void mergeParallelRight(VALUE* array, int size, VALUE* tmp) {
    int idx = size/2-1;
    int jdx = size-1;
    for(int tdx = size-1; tdx >= size/2; tdx--) {
        bool x = (array[idx] > array[jdx]);
        bool y = !x;
        tmp[tdx] = array[idx] * x;
        idx -= 1*x;
        tmp[tdx] += array[jdx] * y;
        jdx -= 1*y;
    }
}



void mergeSortSerial(VALUE* array, int size, VALUE* tmp) {
    if(size == 1) {
        return;
    }
    mergeSortSerial(array, size/2, tmp);
    mergeSortSerial(&array[size/2], size-(size/2), tmp);

    mergeSerial(array, size, tmp);
}


void mergeParallel(VALUE* array, int size, VALUE* tmp) {

#pragma omp parallel sections
    {
#pragma omp section
        mergeParallelLeft(array, size, tmp);
#pragma omp section
        mergeParallelRight(array, size, tmp);
    };
    memcpy(array, tmp, sizeof(VALUE)*size);
}


void mergeSortParallel(VALUE* array, int size, VALUE* tmp, int threadCount) {
    if(threadCount == 1) {
        mergeSortSerial(array, size, tmp);
        return;
    }
#pragma omp parallel sections
    {
#pragma omp section
        mergeSortParallel(array, size/2, tmp, threadCount/2);
#pragma omp section
        mergeSortParallel(&array[size/2], size - (size/2), &tmp[size/2], threadCount - threadCount/2);

    };

    mergeParallel(array, size, tmp);

}

 void mergeSortParallel(VALUE* array, int size) {
    VALUE* tmp = new VALUE[size];
    omp_set_nested(true);
    mergeSortParallel(array, size, tmp, omp_get_max_threads());
}

void mergeSortSerial(VALUE* array, int size) {
    VALUE* tmp = new VALUE[size];
    mergeSortSerial(array, size, tmp);
}

