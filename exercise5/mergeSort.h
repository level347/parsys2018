#ifndef EXERCISE4_MERGESORT_H
#define EXERCISE4_MERGESORT_H

#define VALUE int32_t

void mergeSortSerial(VALUE* array, int size);
void mergeSortParallel(VALUE* array, int size);
void fillRandomized(VALUE* array, int size);
bool isSorted(VALUE* array, int size);

#endif //EXERCISE4_MERGESORT_H
