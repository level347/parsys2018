#!/bin/bash

files=( "./cmake-build-release/nQueens" "./cmake-build-release/mergeSort" "./cmake-build-release/piEstimation" )
nQueensProblemSizes=( "8" "10" "11" "12" "13" "14")
mergeSortProblemSizes=( "2500000" "5000000" "10000000" "25000000" "100000000")
piEstimationProblemSizes=("1000000" "10000000" "25000000" "50000000" "100000000")

for i in "${nQueensProblemSizes[@]}"
do
    echo ${files[0]} $i $1
    for j in 1 2 3 4 5
    do
        ${files[0]} $i $1
    done
done

for i in "${mergeSortProblemSizes[@]}"
do
    echo ${files[1]} $i $1
    for j in 1 2 3 4 5
    do
        ${files[1]} $i $1
    done
done

for i in "${piEstimationProblemSizes[@]}"
do
    echo ${files[2]} $i $1
    for j in 1 2 3 4 5
    do
        ${files[2]} $i $1
    done
done

