#include <iostream>
#include <omp.h>

#include "mergeSort.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    if(argc < 2) {
        return EXIT_FAILURE;
    }
    int size = atoi(argv[1]);
    int num_threads = -1;
    if(argc >= 3) {
        num_threads = atoi(argv[2]);
    }

    VALUE* array = new VALUE[size];
    fillRandomized(array, size);

    if(num_threads < 0) {
        {
            ChronoTimer timer("Serial Merge Sort");
            mergeSortSerial(array, size);
        }
        if(isSorted(array, size)) {
            std::cout << "Array is sorted. " << std::endl;
        }
        else {
            std::cout << "Array is not sorted. " << std::endl;
        }
    }
    else {
        if(num_threads != 0) {
            omp_set_num_threads(num_threads);
        }
        {
            ChronoTimer timer("Parallel Merge Sort");
            mergeSortParallel(array, size);
        }
        if(isSorted(array, size)) {
            std::cout << "Array is sorted. " << std::endl;
        }
        else {
            std::cout << "Array is not sorted. " << std::endl;
        }
    }

}