#ifndef EXERCISE9_JAKOBI2D_H
#define EXERCISE9_JAKOBI2D_H

#include <cmath>
#include <mpi.h>
#include "matrix.h"
#include "stencil.h"

enum messageType {
    northToSouth,
    southToNorth,
    eastToWest,
    westToEast
};

template <int Points>
double sumStencil2D(const Matrix2D<double>& mat, const int i, const int j, const Stencil2D<Points>& stencil) {
    double sum = 0;
    for(uint64_t p = 0; p < Points; p++) {
        auto x = stencil.x[p];
        auto y = stencil.y[p];
        sum += mat[i+y][j+x];
    }
    return sum / Points;
}



template <int Points>
int64_t jakobi2D(Matrix2D<double>& mat, const Stencil2D<Points> stencil, const Border2D<double>& border, const double epsilon, int rank, int blockX, int blockY, int maxBlockX, int maxBlockY, int procCount) {
    auto height = (int)mat.size();
    auto width = (int)mat[0].size();

    auto matSwap = createMatrix<double>(width, height);

    double changes = 0.0;
    Matrix2D<double>* prevMat = &mat;
    Matrix2D<double>* nextMat = &matSwap;

    int32_t cont = 1;
    int changeCount = 0;

    MPI_Request requestWest, requestEast, requestNorth, requestSouth;


    auto* northBorderSend = new double[width];
    auto* southBorderSend = new double[width];
    auto* eastBorderSend = new double[height];
    auto* westBorderSend = new double[height];

    auto* northBorderRcv = new double[width];
    auto* southBorderRcv = new double[width];
    auto* eastBorderRcv = new double[height];
    auto* westBorderRcv = new double[height];

    double* changeValues = nullptr;
    if(rank == 0) {
        changeValues = new double[procCount];
    }


    //Copy external borders
    //West
    if(blockX == 0) {
        //std::cout << "FillWest: " << std::endl;
        for(int i = 1; i < height-1; i++) {
            (*prevMat)[i][0] = border.west;
            (*nextMat)[i][0] = border.west;
        }
    }
    //North
    if(blockY == 0) {
        //std::cout << "FillNorth: " << std::endl;
        for(int i = 1; i < width-1; i++) {
            (*prevMat)[0][i] = border.north;
            (*nextMat)[0][i] = border.north;
        }
    }
    //East
    if(blockX == maxBlockX-1) {
        // std::cout << "FillEast: " << std::endl;
        for(int i = 1; i < height-1; i++) {
            (*prevMat)[i][width-1] = border.east;
            (*nextMat)[i][width-1] = border.east;
        }
    }
    //South
    if(blockY == maxBlockY-1) {
        // std::cout << "FillSouth: " << std::endl;
        for(int i = 1; i < width-1; i++) {
            (*prevMat)[height-1][i] = border.south;
            (*nextMat)[height-1][i] = border.south;
        }
    }

    //std::cout << "Before It: " << std::endl;

    int iterationCount = 0;
    do {
        //std::cout << "Inner Block: " << std::endl;
        //CALC INNER BLOCK
        //CALC INNER CHANGE
        changes = 0.0;
        for(int i = 2; i < height - 2; i++) {
            for(int j = 2; j < width - 2; j++) {
                (*nextMat)[i][j] = sumStencil2D<Points>(*prevMat, i, j, stencil);
                changes += std::abs((*nextMat)[i][j] - (*prevMat)[i][j]);
                changeCount++;
            }
        }
        //std::cout << "Innerchange: " << changes << std::endl;

        //std::cout << "Before Broadcast: " << std::endl;
        //MASTER: SEND CONT
        //SLAVE: RECEIVE MASTER BLOCKING
        MPI_Bcast(&cont, 1, MPI_INT32_T, 0, MPI_COMM_WORLD);

        if(cont == 0) {
            break;
        }

        //std::cout << "Receive Borders: " << std::endl;
        //BLOCK UNTIL RECEIVED (not in iteration 0)

        if(iterationCount != 0) {
            //West
            if(blockX > 0) {
                //std::cout << "Receive West  " << blockX << ", " << blockY  << " from: " <<  rank-1 << " with size " << height << std::endl;
                MPI_Recv(westBorderRcv, height, MPI_DOUBLE, rank-1, eastToWest, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                for(int i = 1; i < height-1; i++) {
                    (*prevMat)[i][0] = westBorderRcv[i];
                }
            }
            //North
            if(blockY > 0) {
                //std::cout << "Receive North  " << blockX << ", " << blockY  << " from: " <<  rank-maxBlockX << " with size " << width << std::endl;
                MPI_Recv(northBorderRcv, width, MPI_DOUBLE, rank-maxBlockX, southToNorth, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                for(int i = 1; i < width-1; i++) {
                    (*prevMat)[0][i] = northBorderRcv[i];
                }
            }
            //East
            if(blockX < maxBlockX-1) {
                //std::cout << "Receive East  " << blockX << ", " << blockY  << " from: " <<  rank+1 << " with size " << height << std::endl;
                MPI_Recv(eastBorderRcv, height, MPI_DOUBLE, rank+1, westToEast, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                for(int i = 1; i < height-1; i++) {
                    (*prevMat)[i][width-1] = eastBorderRcv[i];
                }
            }
            //South
            if(blockY < maxBlockY-1) {
                //std::cout << "Receive South  " << blockX << ", " << blockY  << " from: " <<  rank+maxBlockX << " with size " << width << std::endl;
                MPI_Recv(southBorderRcv, width, MPI_DOUBLE, rank+maxBlockX, northToSouth, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                for(int i = 1; i < width-1; i++) {
                    (*prevMat)[height-1][i] = southBorderRcv[i];
                }
            }
        }

        //std::cout << "Calc Outer: " << std::endl;
        //CALC BORDER
        //CALC TOTAL CHANGE
        for(int i = 2; i < height - 2; i++) {
            //West
            (*nextMat)[i][1] = sumStencil2D<Points>(*prevMat, i, 1, stencil);
            changes += std::abs((*nextMat)[i][1] - (*prevMat)[i][1]);
            changeCount++;

            //East
            (*nextMat)[i][width-2] = sumStencil2D<Points>(*prevMat, i, width-2, stencil);
            changes += std::abs((*nextMat)[i][width-2] - (*prevMat)[i][width-2]);
            changeCount++;
        }

        for(int j = 1; j < width - 1; j++) {
            //North
            (*nextMat)[1][j] = sumStencil2D<Points>(*prevMat, 1, j, stencil);
            changes += std::abs((*nextMat)[1][j] - (*prevMat)[1][j]);
            changeCount++;

            //South
            (*nextMat)[height-2][j] = sumStencil2D<Points>(*prevMat, height-2, j, stencil);
            changes += std::abs((*nextMat)[height-2][j] - (*prevMat)[height-2][j]);
            changeCount++;
        }


        //std::cout << "Gather Changes: " << std::endl;
        //SLAVE: SEND TOTAL CHANGE TO MASTER ASYNC
        //MASTER: RECEIVE CHANGES
        //std::cout << "Local Change: " << changes << std::endl;

        MPI_Gather(&changes, 1, MPI_DOUBLE, changeValues, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        if(rank == 0) {
            //changeValues[0] = changes;
            changes = 0.0;
            for(int i = 0; i < procCount; i++) {
                //std::cout << changeValues[i] << " + ";
                changes += changeValues[i];
            }
            //std::cout << std::endl;
            //std::cout << "Total Changes: " << changes << std::endl;
        }


        //std::cout << "Send Borders: " << std::endl;
        //SEND BORDERS ASYNC

        //West
        if(blockX > 0) {
            //std::cout << "Send West" << std::endl;
            if(iterationCount > 0) {
                MPI_Wait( &requestWest, nullptr );
            }

            for(int i = 1; i < height-1; i++) {
                westBorderSend[i] = (*nextMat)[i][1];
            }
            //std::cout << "Send West from " << blockX << ", " << blockY  << " to: " <<  rank-1 << " with size " << height << std::endl;
            MPI_Isend(westBorderSend, height, MPI_DOUBLE, rank-1, westToEast,MPI_COMM_WORLD, &requestWest);
        }
        //North
        if(blockY > 0) {
            //std::cout << "Send North" << std::endl;
            if(iterationCount > 0) {
                MPI_Wait( &requestNorth, nullptr );
            }
            for(int i = 1; i < width-1; i++) {
                northBorderSend[i] = (*nextMat)[1][i];
            }
            //std::cout << "Send North from " << blockX << ", " << blockY  << " to: " <<  rank-maxBlockX << " with size " << width << std::endl;
            MPI_Isend(northBorderSend, width, MPI_DOUBLE, rank-maxBlockX, northToSouth ,MPI_COMM_WORLD, &requestNorth);
        }
        //East
        if(blockX < maxBlockX-1) {
            //std::cout << "Send East" << std::endl;
            if(iterationCount > 0) {
                MPI_Wait( &requestEast, nullptr );
            }
            for(int i = 1; i < height-1; i++) {
                eastBorderSend[i] = (*nextMat)[i][width-2];
            }
            //std::cout << "Send East from " << blockX << ", " << blockY  << " to: " <<  rank+1 << " with size " << height << std::endl;
            MPI_Isend(eastBorderSend, height, MPI_DOUBLE, rank+1, eastToWest,MPI_COMM_WORLD, &requestEast);
        }
        //South
        if(blockY < maxBlockY-1) {
            //std::cout << "Send South" << std::endl;
            if(iterationCount > 0) {
                MPI_Wait( &requestSouth, nullptr );
            }
            for(int i = 1; i < width-1; i++) {
                southBorderSend[i] = (*nextMat)[height-2][i];
            }
            //std::cout << "Send South from " << blockX << ", " << blockY  << " to: " <<  rank+maxBlockX << " with size " << width << std::endl;
            MPI_Isend(southBorderSend, width, MPI_DOUBLE, rank+maxBlockX, southToNorth ,MPI_COMM_WORLD, &requestSouth);
        }

        iterationCount++;
        auto temp = prevMat;
        prevMat = nextMat;
        nextMat = temp;

        //std::cout << "Iteration count: " << iterationCount << std::endl;
        //std::cout << "ChangeCount: " << changeCount << std::endl;
        changeCount = 0;

        if(rank == 0) {
            cont = changes > epsilon;
        }
        //std::cout << "Loop Body End: " << std::endl;
    } while(true);

    return iterationCount;
}

template <int Points>
int64_t jakobi2DMpi(const Stencil2D<Points> stencil, const Border2D<double>& border, const double epsilon, int size, int rank, int procCount) {

    int hoz = size;
    int vert = size;
    int maxX = 1;
    int maxY = 1;

    for(int64_t i = procCount, cnt = 0; i > 1; i/=2, cnt++) {
        if(cnt%2 == 0) {
            vert /= 2;
            maxY*=2;
        }
        else {
            hoz /= 2;
            maxX*=2;
        }
    }

    int x = rank%maxX;
    int y = rank/maxX;

    /*std::cout << "hoz: " << hoz << std::endl;
    std::cout << "vert: " << vert << std::endl;
    std::cout << "maxX: " << maxX << std::endl;
    std::cout << "maxY: " << maxY << std::endl;
    std::cout << "x: " << x << std::endl;
    std::cout << "y: " << y << std::endl;*/

    auto mat = createMatrix<double>((size_t)hoz+2, (size_t)vert+2);

    return jakobi2D<Points>(mat,stencil, border, epsilon, rank, x, y, maxX, maxY, procCount);
}

#endif //EXERCISE9_JAKOBI2D_H
