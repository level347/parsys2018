#Exercise 9

##Implementation

To prevent the processes from waiting for ghost cell synchronization, the inner block is calculated during the 
asynchronous ghost cell exchange. The epsilon calculation is performed on the master, the receiving of the result
is also done asynchronously and hidden by the calculation of the inner cells. 
Since the problem sizes are quadratic, we always split one dimenison of the 2d room into twice as many blocks as before.
The splitting is done alternating horizontally and vertically. For this reason our implementation only works for the
given problem sizes (=power of two).

##Benchmark Setup

* LCC2
* always exclusively reserved nodes independent of used core count
* 4GB of Ram reserved
* 5 repetitions
* mean value as result

##Benchmark Result

| N    | eps   | 1     | 2     | 4     | 8     | 16    | 32    | 64    | iterations |
|------|-------|-------|-------|-------|-------|-------|-------|-------|------------|
| 512  | 10.0  | 1.144 | 0.594 | 0.301 | 0.159 | 0.140 | 0.181 | 0.384 | 578        |
| 768  | 100.0 | 0.061 | 0.048 | 0.015 | 0.008 | 0.046 | 0.047 | 0.053 | 15         |
| 1024 | 100.0 | 0.182 | 0.149 | 0.082 | 0.022 | 0.051 | 0.050 | 0.060 | 26         |

The benchmark results show that the implementation scales well up to 8 cores on a single node. However,
with a second node, the performance drops depending on the problem size. At this point the performance
stagnates with additional nodes.

Compared to the results of groups 1, 3, 6 and 8 (available results as of now) our implementation performs well for
both low and high number of cores. There are better implementations for low or high processor count each, but
those implementations do not perform as well as our implementation for both regions.