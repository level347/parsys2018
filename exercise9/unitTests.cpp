#include <iostream>
#include <cstdint>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <iterator>

#include "chrono_timer.h"
#include "matrix.h"
#include "stencil.h"
#include "jakobi2D.h"

typedef bool(*testFunction)();



bool stencil2DTest1Proc() {

    if(system("mpirun -np 1 benchmark 512 10.0 578 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest2Proc() {

    if(system("mpirun -np 2 benchmark 512 10.0 578 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest4Proc() {

    if(system("mpirun -np 4 benchmark 512 10.0 578 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest8Proc() {

    if(system("mpirun -np 8 benchmark 512 10.0 578 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest16Proc() {

    if(system("mpirun -np 16 benchmark 512 10.0 578 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest32Proc() {

    if(system("mpirun -np 32 benchmark 512 10.0 578 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest64Proc() {

    if(system("mpirun -np 64 benchmark 512 10.0 578 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest1Proc2() {

    if(system("mpirun -np 1 benchmark 768 100.0 15 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest2Proc2() {

    if(system("mpirun -np 2 benchmark 768 100.0 15 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest4Proc2() {

    if(system("mpirun -np 4 benchmark 768 100.0 15 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest8Proc2() {

    if(system("mpirun -np 8 benchmark 768 100.0 15 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest16Proc2() {

    if(system("mpirun -np 16 benchmark 768 100.0 15 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest32Proc2() {

    if(system("mpirun -np 32 benchmark 768 100.0 15 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest64Proc2() {

    if(system("mpirun -np 64 benchmark 768 100.0 15 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest1Proc3() {

    if(system("mpirun -np 1 benchmark 1024 100.0 26 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest2Proc3() {

    if(system("mpirun -np 2 benchmark 1024 100.0 26 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest4Proc3() {

    if(system("mpirun -np 4 benchmark 1024 100.0 26 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest8Proc3() {

    if(system("mpirun -np 8 benchmark 1024 100.0 26 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest16Proc3() {

    if(system("mpirun -np 16 benchmark 1024 100.0 26 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest32Proc3() {

    if(system("mpirun -np 32 benchmark 1024 100.0 26 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool stencil2DTest64Proc3() {

    if(system("mpirun -np 64 benchmark 1024 100.0 26 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}


std::pair<testFunction, std::string> tests[] = {{stencil2DTest1Proc, "Stencil 2D Test 1 - 1 Process"},
                                                {stencil2DTest2Proc, "Stencil 2D Test 1 - 2 Process"},
                                                {stencil2DTest4Proc, "Stencil 2D Test 1 - 4 Process"},
                                                {stencil2DTest8Proc, "Stencil 2D Test 1 - 8 Process"},
                                                {stencil2DTest16Proc, "Stencil 2D Test 1 - 16 Process"},
                                                {stencil2DTest32Proc, "Stencil 2D Test 1 - 32 Process"},
                                                {stencil2DTest64Proc, "Stencil 2D Test 1 - 64 Process"},
                                                {stencil2DTest1Proc2, "Stencil 2D Test 2 - 1 Process"},
                                                {stencil2DTest2Proc2, "Stencil 2D Test 2 - 2 Process"},
                                                {stencil2DTest4Proc2, "Stencil 2D Test 2 - 4 Process"},
                                                {stencil2DTest8Proc2, "Stencil 2D Test 2 - 8 Process"},
                                                {stencil2DTest16Proc2, "Stencil 2D Test 2 - 16 Process"},
                                                {stencil2DTest32Proc2, "Stencil 2D Test 2 - 32 Process"},
                                                {stencil2DTest64Proc2, "Stencil 2D Test 2 - 64 Process"},
                                                {stencil2DTest1Proc3, "Stencil 2D Test 3 - 1 Process"},
                                                {stencil2DTest2Proc3, "Stencil 2D Test 3 - 2 Process"},
                                                {stencil2DTest4Proc3, "Stencil 2D Test 3 - 4 Process"},
                                                {stencil2DTest8Proc3, "Stencil 2D Test 3 - 8 Process"},
                                                {stencil2DTest16Proc3, "Stencil 2D Test 3 - 16 Process"},
                                                {stencil2DTest32Proc3, "Stencil 2D Test 3 - 32 Process"},
                                                {stencil2DTest64Proc3, "Stencil 2D Test 3 - 64 Process"},};

int main() {
   int n = sizeof(tests)/sizeof(tests[0]);
    int failedTests = 0;
    for(int idx = 0; idx < n; idx++) {
        auto executionTime = 0;
        bool result = 0;
        try {
            ChronoTimer timer("MPI Stencil Benchmark");
            result = tests[idx].first();
            executionTime = timer.elapsedTime();
        }
        catch (std::exception& e) {
            result = 1;
        }
        if(result) {
            std::cout << "\033[1;31mTest failed: " << tests[idx].second << "\033[0m" << std::endl;
            failedTests++;
        }
        else {
            std::cout << "\033[1;32mTest passed: " << tests[idx].second << " " << executionTime << " " << "\033[0m" << std::endl;
        }
    }
    if(failedTests == 0) {
        std::cout << "\033[1;32m" << n-failedTests << " of " << n << " tests completed successfully" << "\033[0m" << std::endl;
    }
    else {
        std::cout << n-failedTests << " of " << n << " tests completed successfully" << std::endl;
    }

}