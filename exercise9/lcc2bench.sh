#!/bin/bash

files=( "./cmake-build-release/benchmark" )

echo ${files[0]} $1
for j in 1 2 3 4 5
do
    mpirun -np $1 ${files[0]} 512 10.0 578
    mpirun -np $1 ${files[0]} 768 100.0 15
    mpirun -np $1 ${files[0]} 1024 100.0 26
done



