//
// Created by bob on 03.12.18.
//

#ifndef EXERCISE7_STENCIL_H
#define EXERCISE7_STENCIL_H

#include <array>

template <std::size_t Points>
struct Stencil2D {
    int8_t x[Points];
    int8_t y[Points];
};

template<typename T>
struct Border2D {
    T north;
    T east ;
    T south;
    T west;
};

#endif //EXERCISE7_STENCIL_H
