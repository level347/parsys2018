#!/bin/bash

#$ -N bve_atr_ex9_par16
#$ -q std.q
#$ -cwd
#$ -l h_rt=00:10:00

# exclusively reserve whatever nodes this is running on
#$ -l excl=true

# reserve 4GB of memory per process
#$ -l h_vmem="4G"

#$ -pe openmpi-8perhost 16

module load gcc/8.2.0
module load openmpi/3.1.1

./lcc2bench.sh 16
