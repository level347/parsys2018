#pragma once

#include <string>
#include <chrono>
#include <iostream>

class ChronoTimer {
    const std::string name;
    const std::chrono::time_point<std::chrono::high_resolution_clock> start;
public:
    ChronoTimer(const std::string& name = "Unnamed")
            : name(name),
              start(std::chrono::high_resolution_clock::now()) {
    }
    int64_t elapsedTime()  {
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start);
        return elapsed.count();
    }
};