
#include <mpi.h>
#include <iostream>
#include <iterator>

#include "matrix.h"
#include "stencil.h"
#include "jakobi2D.h"
#include "chrono_timer.h"

int main(int argc, char** argv) {
    int rank, procCount;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procCount);


    if(argc < 3) {
        return EXIT_FAILURE;
    }
    const auto size = strtol(argv[1], nullptr, 10);
    const auto epsilon = strtod(argv[2], nullptr);

    int64_t expectedIterationCount = 0;
    if(argc >= 4) {
        expectedIterationCount = strtol(argv[3], nullptr, 10);
    }


    Stencil2D<5> stencil= {
        {0, 0, 0, 1, -1,}, // x values
        {-1, 0, 1, 0, 0,} // y values
    };

    Border2D<double> border {
        1.0, 0.5, 0.0, -0.5 // north, east, south, west
    };

    MPI_Barrier(MPI_COMM_WORLD);
    ChronoTimer timer("MPI Stencil Benchmark");
    auto iterationCount = jakobi2DMpi<5>(stencil, border, epsilon, (int)size, rank, procCount);
    MPI_Barrier(MPI_COMM_WORLD);
    auto wallTime = timer.elapsedTime();
    if(rank == 0) {
        std::cout << "Time elapsed: " << wallTime << std::endl;
    }

    MPI_Finalize();

    if(expectedIterationCount > 0) {
        std::cout << iterationCount << std::endl;
        if(iterationCount == expectedIterationCount) {
            return EXIT_SUCCESS;
        }
        else {
            return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}
