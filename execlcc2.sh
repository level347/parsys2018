if [ ! -f "$1" ]; then
    RESULT=$(ssh "$1@lcc2" "$2")
    echo $?
else
    RESULT=$(ssh -i "$1" "$1@lcc2" "$2")
    echo $?
fi
