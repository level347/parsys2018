#Exercise 2
##Part 1
###Expectations
In terms of initialization, we expect both  contiguous variants to be much faster. This is because the contiguous variant 
without indirection needs only 1 allocation, while the variant with indirection needs 2 allocations. In contrast the nested 
vector needs n+1 allocations. This is likely to speedup multiplication with smaller matrix sizes, because the cubic scaling
of the matrix multiplication has not overtaken the linear but expensive memory allocations at these low sizes.

We also expect the contiguous versions to provide better cache behaviour for smaller matrices. This is because loading a
cache line at the end of a row includes some parts of the next row given that the rows are not padded to be of multiple
size of cache lines (which might make sense for vectorization). This might result in fewer cache misses and thus help achieving
better performance. Hardware based prefetching for sequential access and memory burst mode might enhance this effect.

One of the disadvantageous properties of the contiguous variant without indirection is the unintuitive way of accessing
elements with indices. This might not be a problem in C++ with overloaded operators, but is problematic in other languages 
like C, that don't contain such a feature.

###Benchmark Regimen Changes

We have changed our benchmark regimen in some ways. To allow for accurate measurement of small problem sizes, we added a
second parameter to the benchmarked programs that controls the inner repetition. This prevents the overhead of starting
and stopping an executable to influence the benchmarks in a noticeable way. The outer repetition count is still present
and was set to 5 for this example because it allows us to use the multitime package which automatically calculates 
deviation, mean, median, min and max values and also provides user and system times.

We have included the creation of the matrices into the inner repetition loop to minimize the influence of already cached
contents.

The inner repetition count varies from problem size to problem size to allow for adequate values. For this exercise, the 
benchmark results are not normalized regarding inner repetition count, since comparison between problem sizes for a single
variant is not intended. Instead, different variants for the same problem sizes should be compared.

###Benchmarking

####System 1

Following Table shows the real time mean for the benchmarks:

| Problem size      |   10  |   50  |  250  |  750  |  1200 |
|-------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count   |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec         | 1.796 | 0.512 | 0.521 | 0.832 | 1.358 |
| Contiguous + Ind. | 1.122 | 0.493 | 0.511 | 0.831 | 1.299 |
| Contiguous        | 1.380 | 0.634 | 0.652 | 1.017 | 1.479 |


Following Table shows the real time standard deviation for the benchmarks:

| Problem size      |   10  |   50  |  250  |  750  |  1200 |
|-------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count   |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec         | 0.028 | 0.002 | 0.004 | 0.030 | 0.097 |
| Contiguous + Ind. | 0.014 | 0.004 | 0.003 | 0.042 | 0.005 |
| Contiguous        | 0.004 | 0.005 | 0.002 | 0.004 | 0.009 |

####System 2

Following Table shows the real time mean for the benchmarks:

| Problem size      |   10  |   50  |  250  |  750  |  1200 |
|-------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count   |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec         | 2.095 | 0.647 | 0.581 | 0.874 | 1.460 |
| Contiguous + Ind. | 1.343 | 0.506 | 0.536 | 0.864 | 1.436 |
| Contiguous        | 1.476 | 0.630 | 0.664 | 1.057 | 1.639 |


Following Table shows the real time standard deviation for the benchmarks:

| Problem size      |   10  |   50  |  250  |  750  |  1200 |
|-------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count   |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec         | 0.002 | 0.003 | 0.000 | 0.002 | 0.009 |
| Contiguous + Ind. | 0.005 | 0.001 | 0.004 | 0.013 | 0.007 |
| Contiguous        | 0.002 | 0.003 | 0.003 | 0.004 | 0.003 |

###Profiling

We use the perf stat command with following events to retrieve a cache hit miss ratio for retired instructions for all cache levels.

* mem_load_uops_retired.l1_hit
* mem_load_uops_retired.l1_miss
* mem_load_uops_retired.l2_hit
* mem_load_uops_retired.l2_miss
* mem_load_uops_retired.l3_hit
* mem_load_uops_retired.l3_miss

L1 cache hit miss ration:

| Problem size      |   10  |   50  |  250  |  750  |  1200 |
|-------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count   |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec         | 0.01% | 0.10% |0.21% | 0.20% | 0.21% |
| Contiguous + Ind. | 0.00% | 0.12% | 0.21% | 0.20% | 0.22% |
| Contiguous        | 0.01% | 0.08% | 0.09% | 0.07% | 0.09% |

L2 cache hit miss ration:

| Problem size      |   10  |   50  |  250  |  750  |  1200 |
|-------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count   |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec         | 194.74% | 9.37% | 42.85% | 38.79% | 76.25% |
| Contiguous + Ind. | 231.42% | 7.31% | 33.64% | 34.38% | 70.65% |
| Contiguous        | 238.48% | 10.63% | 23.54% | 19.70% | 43.33% |

L3 cache hit miss ration:

| Problem size      |   10  |   50  |  250  |  750  |  1200 |
|-------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count   |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec         | 11.89% | 6.69% | 2.75% | 12.45% | 152.79% |
| Contiguous + Ind. | 11.90% | 11.95% | 2.53% | 13.82% | 150.31% |
| Contiguous        | 12.85% | 9.52% | 10.68% | 30.13% | 111.51% |


####Discussion

For small problems sizes the nested vec implementation is significantly slower than the other variants. This might be 
because of points we mentioned in the assumptions. However, it also shows that the contiguous variant without indirection
is a slower than the version with indirection across all problem sizes. We are not sure what causes this effect.
At medium and larger sizes, the variants with indirection provide about the same performance.

The data gained from profiling is quite difficult to interpret. L1 cache misses seem to occur quite rarely in comparison
to L1 cache hits. Since L1 cache misses are not that big of a deal, only little information can be retrieved from those metrics.

In regard to problem size, the L3 cache misses soar at a problem size of 1200, as expected because the matrices are too
big for the L3 cache. 
Another quite interesting effect is that the cache misses occur more often on smaller and bigger
matrix sizes, but more rarely on medium ones. We think this effect can be explained by the count data elements are reused.
On small matrices, data is loaded once (=cache misses) and reused rarely. On medium sizes the elements are reused more 
often (cubic complexity). On large data sets, data is evicted because the cache is too small.

The comparison between the different data structures based on this metric shows some contracdictions with the benchmark
results. E.g. The contiguous variant at problem size 1200 has lowest L3, L2 and L1 cache misses, but still the worst
benchmark result.

One possible measurement error is due to the possible reuse of already cached memory. In the case the same memory location
is reused between multiple iterations because the same region was allocated, the data location is already cached.

##Part 2

###Optimization

For this example we altered the loop bound for index k. Usually the loop would iterate until k = n. Since we have an upper
triangular matrix we are now able to omit all iterations for this loop where k > i. Therefore, we just changed the
bound from k < n to k < i+1.

###OpenMP

Since the work per iteration of the most outer loop is now not constant anymore, static scheduling is not a good idea, as
it will not provide the speedup that we could see. We changed OpenMPs scheduling strategy from default static to dynamic via
the schedule(dynamic) clause, to achieve better workload distribution. 

###Benchmarking

####System 1

Following Table shows the real time mean for the benchmarks:

| Problem Size     |   10  |   50  |  250  |  750  |  1200 |
|------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count  |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec        | 1.763 | 0.510 | 0.520 | 0.805 | 1.333 |
| UpperTri         | 1.541 | 0.305 | 0.278 | 0.409 | 0.614 |
| UpperTriOmp      | 5.211 | 0.284 | 0.158 | 0.247 | 0.498 |
| UpperTriOmpSched | 5.755 | 0.371 | 0.185 | 0.245 | 0.444 |

Following Table shows the real time standard deviation for the benchmarks:

| Problem Size     |   10  |   50  |  250  |  750  |  1200 |
|------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count  |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec        | 0.014 | 0.003 | 0.002 | 0.010 | 0.058 |
| UpperTri         | 0.024 | 0.004 | 0.004 | 0.003 | 0.014 |
| UpperTriOmp      | 0.252 | 0.013 | 0.006 | 0.012 | 0.071 |
| UpperTriOmpSched | 0.617 | 0.020 | 0.016 | 0.008 | 0.020 |

####System 2

Following Table shows the real time mean for the benchmarks:

| Problem Size     |   10  |   50  |  250  |  750  |  1200 |
|------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count  |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec        | 2.099 | 0.646 | 0.582 | 0.870 | 1.373 |
| UpperTri         | 1.799 | 0.392 | 0.312 | 0.455 | 0.644 |
| UpperTriOmp      | 9.589 | 0.455 | 0.165 | 0.171 | 0.293 |
| UpperTriOmpSched | 9.855 | 0.487 | 0.153 | 0.174 | 0.272 |

Following Table shows the real time standard deviation for the benchmarks:

| Problem Size     |   10  |   50  |  250  |  750  |  1200 |
|------------------|:-----:|:-----:|:-----:|:-----:|:-----:|
| Inner rep count  |  10^6 |  5000 |   50  |   3   |   1   |
| NestedVec        | 0.007 | 0.003 | 0.004 | 0.003 | 0.018 |
| UpperTri         | 0.013 | 0.001 | 0.000 | 0.002 | 0.004 |
| UpperTriOmp      | 0.250 | 0.027 | 0.020 | 0.013 | 0.007 |
| UpperTriOmpSched | 0.160 | 0.015 | 0.005 | 0.007 | 0.008 |

####Discussion

The results show that for small matrices the non-OMP versions are significantly faster. This is because of the high repetition 
count and the resulting massive creation of threads for these small workloads. 
The variant with the upper tri optimization performs
always better than the version without. At small sizes this difference is small, most likely because the bypassed values are
cached anyway. At larger problem sizes the speedup reaches about factor 2 which was expected.
OpenMP with static scheduling provides a substantial speedup against the non-OpenMP version at medium sizes. This speedup
shrinks again at larger sizes, most likely to the influence of cache size, which occur because no tiling is used.
The variant with dynamic scheduling is a bit slower than the version with static scheduling on medium sizes. This might
just be the overhead of dynamic scheduling. On larger problem sizes the dynamic scheduled version overtakes the static
scheduled one as expected.

The standard deviation is quite low, except for the OpenMp versions at small matrix size. This indicates that the creation
time of threads varies quite a lot.