./perf.sh exercise2NestedVec 10 1000000
./perf.sh exercise2NestedVec 50  5000
./perf.sh exercise2NestedVec 250 50
./perf.sh exercise2NestedVec 750 3
./perf.sh exercise2NestedVec 1200 1
./perf.sh exercise2ContIndirection 10 1000000
./perf.sh exercise2ContIndirection 50  5000
./perf.sh exercise2ContIndirection 250 50
./perf.sh exercise2ContIndirection 750 3
./perf.sh exercise2ContIndirection 1200 1
./perf.sh exercise2Contiguous 10 1000000
./perf.sh exercise2Contiguous 50  5000
./perf.sh exercise2Contiguous 250 50
./perf.sh exercise2Contiguous 750 3
./perf.sh exercise2Contiguous 1200 1
