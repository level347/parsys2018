if [ -z "$3" ]; then
    ./execlcc2.sh "$2" "rm -rf './$1' && tar -xvzf '$1.tar.gz' && cd './$1' && mkdir -p cmake-build-release && cd ./cmake-build-release && cmake -DCMAKE_BUILD_TYPE=Release ../ && make"
else
    ./execlcc2.sh "$2" "module load $3 && rm -rf './$1' && tar -xvzf '$1.tar.gz' && cd './$1' && mkdir -p cmake-build-release && cd ./cmake-build-release && cmake -DCMAKE_BUILD_TYPE=Release ../ && make"
fi



