tar cfvz "$1.tar.gz" "$1"
if [ ! -f "$2" ]; then
    scp "$1.tar.gz" "$2@lcc2.uibk.ac.at:/home/cb01/$2/$1.tar.gz"
else    
    scp -i "$2" "$1.tar.gz" "$2@lcc2.uibk.ac.at:/home/cb01/$2/$1.tar.gz"
fi

