#Exercise 8

For this exercise we only implemented the pi estimation MPI version. We did not implement the MPI stencil version.

##Benchmark Setup

We LCC2 to run MPI jobs with 1 and 2 processes to show that the given requirement of scaling > 100% is fulfilled. We
used the problem sizes 10M, 50M, 100M to get execution times below one second. We repeated each benchmark 5 times and
used the median value as result.


##Benchmark Results

|            | 10M | 50M | 100M |
|------------|--------|--------|--------|
| MPI 1     |   54      |    283         |   559       |
| MPI 2     |   26     |   135        |   275     |
| Speedup    |    200%    |    210%    |   203%     |

##Benchmark Discussion

The benchmark results show that the example scales perfectly. Since the underlying problem is quite simple, this
is exactly the behavior we would have expected.
