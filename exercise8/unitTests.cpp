#include <iostream>
#include <cstdint>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <iterator>
#include <unistd.h>


typedef bool(*testFunction)();

bool compareFiles(const std::string& p1, const std::string& p2) {
    std::ifstream f1(p1, std::ifstream::binary|std::ifstream::ate);
    std::ifstream f2(p2, std::ifstream::binary|std::ifstream::ate);

    if (f1.fail() || f2.fail()) {
        return false; //file problem
    }

    if (f1.tellg() != f2.tellg()) {
        return false; //size mismatch
    }

    //seek back to beginning and use std::equal to compare contents
    f1.seekg(0, std::ifstream::beg);
    f2.seekg(0, std::ifstream::beg);
    return std::equal(std::istreambuf_iterator<char>(f1.rdbuf()),
                      std::istreambuf_iterator<char>(),
                      std::istreambuf_iterator<char>(f2.rdbuf()));
}

bool piEstimationTest1Proc() {

    if(system("mpirun -np 1 estimatePiMpi 10000000 0.01 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool piEstimationTest2Proc() {

    if(system("mpirun -np 2 estimatePiMpi 10000000 0.01 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

bool piEstimationTest4Proc() {

    if(system("mpirun -np 4 estimatePiMpi 10000000 0.01 > /dev/null") != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

std::pair<testFunction, std::string> tests[] = {{piEstimationTest1Proc, "Pi Estimation MPI test with 1 proc"},
                                                {piEstimationTest2Proc, "Pi Estimation MPI test with 2 proc"},
                                                {piEstimationTest4Proc, "Pi Estimation MPI test with 4 proc"},};

int main() {
   int n = sizeof(tests)/sizeof(tests[0]);
    int failedTests = 0;
    for(int idx = 0; idx < n; idx++) {
        bool result = false;
        try {
            result = tests[idx].first();
        }
        catch (std::exception& e) {
            result = true;
        }
        if(result) {
            std::cout << "\033[1;31mTest failed: " << tests[idx].second << "\033[0m" << std::endl;
            failedTests++;
        }
    }
    if(failedTests == 0) {
        std::cout << "\033[1;32m" << n-failedTests << " of " << n << " tests completed successfully" << "\033[0m" << std::endl;
    }
    else {
        std::cout << n-failedTests << " of " << n << " tests completed successfully" << std::endl;
    }

}