#include <mpi.h>
#include <cstdio>
#include <omp.h>
#include <cmath>

#include "piEstimation.h"
#include "chrono_timer.h"

int masterProcess(int sampleCount, int rank, int procCount, double epsilon);
void slaveProcess(int sampleCount, int rank, int procCount);

constexpr uint32_t startingSeed = 107;

int main(int argc, char** argv) {
    int rank, procCount;
    int success = EXIT_SUCCESS;

    if(argc < 2) {
        return EXIT_FAILURE;
    }
    int sampleCount = std::stoi(argv[1]);

    double epsilon = 0.0;
    if(argc >= 3) {
        epsilon = std::stod(argv[2]);
    }

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procCount);

    if(rank == 0) {
        success = masterProcess(sampleCount, rank, procCount, epsilon);
    } else {
        slaveProcess(sampleCount, rank, procCount);
    }

    MPI_Finalize();
    return success;
}

int masterProcess(int sampleCount, int rank, int procCount, double epsilon) {
    auto seed = startingSeed + rank;
    auto procSampleCount = sampleCount / procCount;
    int64_t data[2];


    // let the master thread perform one slice of the work, before it receives the data of other processes
    ChronoTimer timer("MPI Master Pi Estimation");
    int64_t inside = estimatePiMPI(procSampleCount, seed);
    double avgElapsedTime = timer.elapsedTime();

    for(int i = 1; i < procCount; i++) {
        MPI_Recv(data, 2, MPI_INT64_T, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        inside += data[0];
        avgElapsedTime = (avgElapsedTime + data[1]) / 2;
    }
    double pi = (double)(inside * 4) / (double)sampleCount;
    double totalElapsedTime = timer.elapsedTime();


    std::cout << "Pi estimation with " << procCount << " processes and " << procSampleCount << " samples per process:" << std::endl;
    std::cout.precision(17);
    std::cout << "Estimated Pi value: " << pi << std::endl;
    std::cout << "Avg time per process: " << avgElapsedTime << " ms" << std::endl;
    std::cout << "Total time: " << totalElapsedTime << " ms" << std::endl;

    if(epsilon != 0.0) {
        return (std::abs(pi)-M_PI > epsilon);
    }
    return EXIT_SUCCESS;
}

void slaveProcess(int sampleCount, int rank, int procCount) {
    auto seed = startingSeed + rank;
    auto procSampleCount = sampleCount / procCount;

    if(rank == procCount-1) {
        procSampleCount = sampleCount - ((sampleCount / procCount) * (procCount-1));
    }

    ChronoTimer timer("MPI Slave Pi Estimation");
    auto elementsInside = estimatePiMPI(procSampleCount, seed);

    int64_t data[2];
    data[1] = timer.elapsedTime();
    data[0] = elementsInside;
    MPI_Send(data, 2, MPI_INT64_T, 0, 0, MPI_COMM_WORLD);
}

