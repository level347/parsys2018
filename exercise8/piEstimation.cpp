#include <mpi.h>
#include <cstdint>
#include "fastrand.h"


int64_t estimatePiMPI(int64_t samples, uint32_t seed) {

    FastRand fastRand(seed);

    int inside = 0;
    for(int idx = 0; idx < samples; idx++){
        float x = (float)fastRand.generate()/(float)UINT32_MAX;
        float y =  (float)fastRand.generate()/(float)UINT32_MAX;
        inside += (x*x+y*y <= 1.0);
    }

    return inside;
}