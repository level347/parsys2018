#!/bin/bash

files=( "./cmake-build-release/estimatePiMpi" )
benchProblemSizes=( "10000000", "50000000", "100000000" )

module load gcc/8.2.0
module load openmpi/3.1.1

for i in "${benchProblemSizes[@]}"
do
    echo ${files[0]} $i
    for j in 1 2 3 4 5
    do
        mpirun -np $1 ${files[0]} $i
    done
done


