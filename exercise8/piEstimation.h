#ifndef EXERCISE4_PIESTIMATION_H
#define EXERCISE4_PIESTIMATION_H

#include <cstdint>

int64_t estimatePiMPI(int64_t samples, uint32_t seed);

#endif //EXERCISE4_PIESTIMATION_H
