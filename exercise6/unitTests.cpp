#include <iostream>
#include <cstdint>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <thread>
#include <omp.h>

#include "avltree.h"
#include "avltree_par.h"

typedef bool(*testFunction)();

constexpr int TREE_SIZE = 1e5;

bool avlRandomInsertionSerial() {
    uint32_t* array = new uint32_t[TREE_SIZE];
    fillRandomized<uint32_t>(array, TREE_SIZE);

    auto avlTreeSerial = AvlTree<uint32_t>{};
    for(int i = 0; i < TREE_SIZE; i++) {
        avlTreeSerial.insert(array[i]);
    }

    uint32_t* sortedArray = new uint32_t[TREE_SIZE];
    memcpy(sortedArray, array, sizeof(uint32_t)*TREE_SIZE);

    std::sort(sortedArray,sortedArray+TREE_SIZE);
    return !avlTreeSerial.verifyItems(sortedArray, TREE_SIZE);
}

void insert(AvlTreePar<uint32_t>* tree, uint32_t* arr, int start, int end) {
    for(int i = start; i < end; i++) {
        tree->insert(arr[i]);
    }
}

bool avlRandomInsertionPar() {
    uint32_t* array = new uint32_t[TREE_SIZE];
    fillRandomized<uint32_t>(array, TREE_SIZE);

    std::vector<std::thread> threads;

    auto avlTreePar = AvlTreePar<uint32_t>{};

    int numThreads = omp_get_max_threads();
    int start = 0;
    int chunck = TREE_SIZE/numThreads;

    int end = chunck;
    for(int i = 0; i < numThreads-1; i++) {
        threads.push_back(std::thread(insert, &avlTreePar, array, start, end));
        end += chunck;
        start += chunck;
    }
    threads.push_back(std::thread(insert, &avlTreePar, array, start, TREE_SIZE));

    for (auto& t : threads) {
        t.join();
    }

    uint32_t* sortedArray = new uint32_t[TREE_SIZE];
    memcpy(sortedArray, array, sizeof(uint32_t)*TREE_SIZE);

    std::sort(sortedArray,sortedArray+TREE_SIZE);
    return !avlTreePar.verifyItems(sortedArray, TREE_SIZE);
}

std::pair<testFunction, std::string> tests[] = {{avlRandomInsertionSerial, "Serial random item insertion"},
                                                {avlRandomInsertionPar, "Parallel random item insertion"}};

int main(void) {
   int n = sizeof(tests)/sizeof(tests[0]);
    int failedTests = 0;
    for(int idx = 0; idx < n; idx++) {
        bool result = 0;
        try {
            result = tests[idx].first();
        }
        catch (std::exception e) {
            result = 1;
        }
        if(result) {
            std::cout << "\033[1;31mTest failed: " << tests[idx].second << "\033[0m" << std::endl;
            failedTests++;
        }
    }
    if(failedTests == 0) {
        std::cout << "\033[1;32m" << n-failedTests << " of " << n << " tests completed successfully" << "\033[0m" << std::endl;
    }
    else {
        std::cout << n-failedTests << " of " << n << " tests completed successfully" << std::endl;
    }

}