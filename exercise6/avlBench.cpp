#include <iostream>
#include <algorithm>
#include <cstring>
#include <omp.h>
#include <thread>

#include "chrono_timer.h"
#include "avltree.h"
#include "avltree_par.h"

void insert(AvlTreePar<uint32_t>* tree, uint32_t* arr, int start, int end) {
    for(int i = start; i < end; i++) {
        tree->insert(arr[i]);
    }
}



int main(int argc, char** argv) {
    if(argc != 2) {
        return EXIT_FAILURE;
    }
    int size = atoi(argv[1]);

    uint32_t* array = new uint32_t[size];
    fillRandomized<uint32_t>(array, size);
    uint32_t* sortedArray = new uint32_t[size];
    memcpy(sortedArray, array, sizeof(uint32_t)*size);
    std::sort(sortedArray,sortedArray+size);

    auto avlTreeSerial = AvlTree<uint32_t>{};
    {
        ChronoTimer timer("Serial AVL Tree insert");
        for(int i = 0; i < size; i++) {
            avlTreeSerial.insert(array[i]);
        }
    }

    if(avlTreeSerial.verifyItems(sortedArray, size)) {
        std::cout << "Serial tree is valid" << std::endl;
    }
    std::vector<std::thread> threads;

    auto avlTreePar = AvlTreePar<uint32_t>{};
    {
        ChronoTimer timer("Parallel AVL Tree insert");

        int numThreads = omp_get_max_threads();
        int start = 0;
        int chunck = size/numThreads;

        int end = chunck;
       // #pragma omp parallel for num_threads(4)
        for(int i = 0; i < numThreads-1; i++) {
            threads.push_back(std::thread(insert, &avlTreePar, array, start, end));
            end += chunck;
            start += chunck;
        }
        threads.push_back(std::thread(insert, &avlTreePar, array, start, size));

        for (auto& t : threads) {
            t.join();
        }

    }

    avlTreePar.printDotFile("test.dot");

    if(avlTreePar.verifyItems(sortedArray, size)) {
        std::cout << "Parallel tree is valid" << std::endl;
    }
}