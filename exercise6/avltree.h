//
// Created by bob on 25.11.18.
//

#ifndef EXERCISE6_AVL_H
#define EXERCISE6_AVL_H

#include <iostream>
#include <vector>
#include <fstream>
#include <random>

static std::mt19937 gen (123);

template <typename T>
void fillRandomized(T* array, int size) {
    std::uniform_int_distribution<T> distrib(0, size / 8);
    for(int idx=0; idx<size; idx++) {
        array[idx] = distrib(gen);
    }
}

template <typename T>
class AvlTree {

    struct Node {
        T value;
        uint16_t height;
        Node* parent;
        Node* left;
        Node* right;

        bool operator< (const Node& rhs) {
            return value < rhs.value;
        }
        bool operator> (const Node& rhs) { return rhs < this; }
        bool operator<=(const Node& rhs) { return !(this > rhs); }
        bool operator>=(const Node& rhs) { return !(this < rhs); }
        bool operator==(const Node& rhs) {
            return value == rhs.value;
        }
        bool operator!=(const Node& rhs) { return !(this == rhs); }
    };

    Node* root;

    bool insertRec(Node* parent, Node* newNode) {
        if(*parent == *newNode) {
            return false; // element is already in list -> ignore
        }
        if(*newNode < *parent) {
            // if there is no left subtree -> insert the node
            // else -> traverse the left subtree
            if(parent->left == nullptr) {
                parent->left = newNode;
                newNode->parent = parent;
                return true;
            } else {
                return insertRec(parent->left, newNode);
            }
        } else {
            // if there is no right subtree -> insert the node
            // else -> traverse the right subtree
            if(parent->right == nullptr) {
                parent->right = newNode;
                newNode->parent = parent;
                return true;
            } else {
                return insertRec(parent->right, newNode);
            }
        }
    }

    int getHeight(Node* node) {
        return node != nullptr ? node->height : 0;
    }

    void rotateLeft(Node* node) {
        Node* x = node;
        Node* y = x->right;
        Node* T2 = y->left;
        y->left = x;
        x->right = T2;
        if(T2) {
            T2->parent = x;
        }
        y->parent = x->parent;
        x->parent = y;
        if(x == this->root) {
            this->root = y;
        } else {
            if(y->parent->left == x) {
                y->parent->left = y;
            } else {
                y->parent->right = y;
            }
        }
    }

    void rotateRight(Node* node) {
        Node* y = node;
        Node* x = y->left;
        Node* T2 = x->right;
        x->right = y;
        y->left = T2;
        if(T2) {
            T2->parent = y;
        }
        x->parent = y->parent;
        y->parent = x;
        if(y == this->root) {
            this->root = x;
        } else {
            if(x->parent->left == y) {
                x->parent->left = x;
            } else {
                x->parent->right = x;
            }
        }
    }

    void updateHeight(Node* node) {
        if(node->left == nullptr && node->right == nullptr) {
            node->height = 0;
            return;
        }
        node->height = static_cast<uint16_t>(std::max(this->getHeight(node->left), this->getHeight(node->right)) + 1);
    }

    void updateHeightRec(Node* node) {
        if(node->left) {
            updateHeightRec(node->left);
        }
        if(node->right) {
            updateHeightRec(node->right);
        }

        updateHeight(node);
    }

    void rebalanceTree(Node* node, int depth) {
        auto leftHeight = this->getHeight(node->left);
        auto rightHeight = this->getHeight(node->right);

        if(rightHeight - leftHeight >= 2) { // right heavy
            auto rn = node->right;
            if(this->getHeight(rn->left) > this->getHeight(rn->right)) { // right-left
                this->rotateRight(rn);
                this->updateHeight(rn);
            }
            this->rotateLeft(node); // right-right, or second part of right-left
        } else if (leftHeight - rightHeight >= 2) { // left heavy
            auto ln = node->left;
            if(this->getHeight(ln->right) > this->getHeight(ln->left)) { // left-right
                this->rotateLeft(ln);
                this->updateHeight(ln);
            }
            this->rotateRight(node); // left-left, or second part of left-right
        }

        this->updateHeight(node);

        if(node->parent != nullptr) {
            this->rebalanceTree(node->parent, depth + 1);
        }
    }

    void printDotFileRec(std::ofstream& outfile, Node* node) {
        if(node->left) {
            outfile << node->value << " -> " << node->left->value << ";" << std::endl;
            printDotFileRec(outfile, node->left);
        }
        if(node->right) {
            outfile << node->value << " -> " << node->right->value << ";" << std::endl;
            printDotFileRec(outfile, node->right);
        }
    }

public:
    AvlTree() {
        this->root = nullptr;
    }

    void insert(T elem) {

        Node* newElem = new Node{};
        newElem->value = elem;
        newElem->parent = nullptr;
        newElem->left = nullptr;
        newElem->right = nullptr;
        newElem->height = 0;

        // if the element is the first node, set it as root
        if(this->root == nullptr) {
            this->root = newElem;
            return;
        }

        if(!this->insertRec(root, newElem)) {
            // element was already in the tree
            return;
        }

        // ensure AVL property
        this->rebalanceTree(newElem, 0);

    }

    void getData(Node* node, std::vector<Node*>& data) {
        if (node == nullptr) {
            return;
        }
        getData(node->left, data);
        data.push_back(node);
        getData(node->right, data);
    }

    bool verifyItems(T* array, int size) {
        auto treeData = std::vector<Node*>();
        treeData.reserve(size);
        getData(this->root, treeData);
        int idx = 0;

        for(auto node : treeData) {
            if(abs(this->getHeight(node->left) - this->getHeight(node->right)) > 1) {
                std::cout << "Tree is not balanced" << std::endl;
                return false;
            }
            if(node->value != array[idx]) {
                std::cout << "Tree value " << node->value << " is not equal to << " << array[idx] << " (idx: "<< idx << ") from verfication values" << std::endl;
                return false;
            }
            while(node->value == array[idx]) {
                idx++;
            }
        }

        return true;
    }

    void printDotFile(const std::string filename) {
        std::ofstream outfile (filename);

        outfile << "digraph avltree" << std::endl;
        outfile << "{" << std::endl;

        printDotFileRec(outfile, this->root);

        outfile << "}" << std::endl;

        outfile.close();
    }


};


#endif //EXERCISE6_AVL_H
