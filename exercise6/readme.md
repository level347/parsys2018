#Exercise 6

#Parallelization Strategy

To parallelize the insert method of the AVL tree we used shared mutexes from C++ instead of OMP locks, because they allow
multiple readers to concurrently hold a lock or alternatively one writer. 
On insert we search for the first unequal node from the root to the point where the new value should be inserted. This
search uses only read locks on each node visited. If the inserted value is not already present in the tree, the
parent of the first unequal node is locked with a write lock. This node was chosen because the effect of any insert
operation can only cause rotations and weight updates until the first unequal node. 
After the lock is acquired the first unequal node is validated. If it is still the same node the new element is inserted
including rebalancing and weight updates.
The current solution still contains a bug that results in invalid trees, which we could not find in reasonable time. 
One possible explanation could be that the problem is caused by two concurrent insertions of the same value if the first 
unequal node is rotated and the second thread is not able to find the value since it searches in wrong subtree.

##Benchmark Setup

We used the problem sizes 125000, 250000, 500000, 1000000 for this example. We used a runtime of about 1 second to
determine the biggest problem size and iteratively halved it to determine the smaller problem sizes.
We are using 3 repetitions for each problem size and determine the final value by calculating the mean.
The benchmarks were performed on LCC2 with 8 threads.

##Benchmark Results

|            | 125000 | 250000 | 500000 | 1000000 |
|------------|--------|--------|--------|---------|
| Serial     |   28     |    63    |   151     |   406      |
| Parallel 8 |   554     |   1138     |   2448     |    5346     |
| Speedup    |    5%    |    6%    |   6%     |   8%     |

##Benchmark Discussion

The results show that the parallel version is not faster at all compared to the serial version. The parallelization
strategy seems to be to inefficient especially for smaller tree sizes. However, with bigger tree sizes the difference
in execution time decreases slightly.