#ifndef EXERCISE4_FASTRAND_H
#define EXERCISE4_FASTRAND_H


#include <cstdint>
#include <cstring>

class FastRand {
    uint32_t seed;
public:
    FastRand(const uint32_t _seed) {
        seed = _seed;

    }

    uint32_t generate() {
        seed = 214013*seed+2531011;
        return seed;
    }

};

class FastRandVec {
    uint32_t seeds[4];
public:
    FastRandVec(const uint32_t* _seeds) {
        memcpy(seeds, _seeds, sizeof(uint32_t)*4 );
    }

    uint32_t* generate() {
        #pragma omp simd
        for(int i = 0; i<4; i++) {
            seeds[i] = 214013*seeds[i]+2531011;
        }

        return seeds;
    }

};

#endif //EXERCISE4_FASTRAND_H
