#!/bin/bash

files=( "./cmake-build-release/benchmark" )
benchmarkProblemSizes=( "125000" "250000" "500000" "1000000" )

for i in "${benchmarkProblemSizes[@]}"
do
    echo ${files[0]} $i
    for j in 1 2 3
    do
        ${files[0]} $i
    done
done


