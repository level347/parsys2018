//
// Created by bob on 25.11.18.
//

#ifndef EXERCISE6_AVL_PAR_H
#define EXERCISE6_AVL_PAR_H

#include <iostream>
#include <vector>
#include <fstream>
#include <random>
#include <omp.h>

#include <mutex>  // For std::unique_lock
#include <shared_mutex>
#include <thread>


template <typename T>
class AvlTreePar {

    struct Node {
        T value;
        uint16_t height;
        Node* parent;
        Node* left;
        Node* right;
        std::shared_mutex rwlock;

        bool operator< (const Node& rhs) {
            return value < rhs.value;
        }
        bool operator> (const Node& rhs) { return rhs < this; }
        bool operator<=(const Node& rhs) { return !(this > rhs); }
        bool operator>=(const Node& rhs) { return !(this < rhs); }
        bool operator==(const Node& rhs) {
            return value == rhs.value;
        }
        bool operator!=(const Node& rhs) { return !(this == rhs); }
    };

    Node* root;
    int lockcount = 0;
    bool writeLocked = false;
    Node* lastLocked = nullptr;
    std::mutex rootlock;

    bool findFirstUnequalNode(Node* parent, Node* newNode, Node** lastUneqalNode) {
        parent->rwlock.lock_shared();
        lockcount++;
        // std::cout << "Locked " << parent << std::endl;

        bool doesNotContainValue;

        if(*lastUneqalNode == nullptr && isUnequalNode(parent)) {
            *lastUneqalNode = parent;
        }
        if(*parent == *newNode) {
            doesNotContainValue = false; // element is already in list -> ignore
        } else if(*newNode < *parent) {
            // if there is no left subtree -> insert the node
            // else -> traverse the left subtree
            if(parent->left == nullptr) {
                if(*lastUneqalNode == nullptr) {
                    *lastUneqalNode = parent;
                }
                doesNotContainValue = true;
            } else {
                doesNotContainValue = findFirstUnequalNode(parent->left, newNode, lastUneqalNode);
            }
        } else {
            // if there is no right subtree -> insert the node
            // else -> traverse the right subtree
            if(parent->right == nullptr) {
                if(*lastUneqalNode== nullptr) {
                    *lastUneqalNode = parent;
                }
                doesNotContainValue = true;
            } else {
                doesNotContainValue = findFirstUnequalNode(parent->right, newNode, lastUneqalNode);
            }
        }

        lockcount--;
        parent->rwlock.unlock_shared();
        // std::cout << "Unlocked " << parent << std::endl;

        return doesNotContainValue;
    }

    bool insertRec(Node* parent, Node* newNode) {
        if(*parent == *newNode) {
            return false; // element is already in list -> ignore
        }
        if(*newNode < *parent) {
            // if there is no left subtree -> insert the node
            // else -> traverse the left subtree
            if(parent->left == nullptr) {
                parent->left = newNode;
                newNode->parent = parent;
                return true;
            } else {
                return insertRec(parent->left, newNode);
            }
        } else {
            // if there is no right subtree -> insert the node
            // else -> traverse the right subtree
            if(parent->right == nullptr) {
                parent->right = newNode;
                newNode->parent = parent;
                return true;
            } else {
                return insertRec(parent->right, newNode);
            }
        }
    }

    int getHeight(Node* node) {
        return node != nullptr ? node->height + 1: 0;
    }

    int isUnequalNode(Node* node) {
        auto left = node->left != nullptr ? node->left->height + 1 : 0;
        auto right = node->right != nullptr ? node->right->height + 1 : 0;
        return abs(left - right) > 0;
    }

    void rotateLeft(Node* node) {
        Node* x = node;
        Node* y = x->right;
        Node* T2 = y->left;
        y->left = x;
        x->right = T2;
        if(T2) {
            T2->parent = x;
        }
        y->parent = x->parent;
        x->parent = y;
        if(x == this->root) {
            this->root = y;
        } else {
            if(y->parent->left == x) {
                y->parent->left = y;
            } else {
                y->parent->right = y;
            }
        }
    }

    void rotateRight(Node* node) {
        Node* y = node;
        Node* x = y->left;
        Node* T2 = x->right;
        x->right = y;
        y->left = T2;
        if(T2) {
            T2->parent = y;
        }
        x->parent = y->parent;
        y->parent = x;
        if(y == this->root) {
            this->root = x;
        } else {
            if(x->parent->left == y) {
                x->parent->left = x;
            } else {
                x->parent->right = x;
            }
        }
    }

    void updateHeight(Node* node) {
        if(node->left == nullptr && node->right == nullptr) {
            node->height = 0;
            return;
        }
        node->height = static_cast<uint16_t>(std::max(this->getHeight(node->left), this->getHeight(node->right)));
    }

    void updateHeightRec(Node* node) {
        if(node->left) {
            updateHeightRec(node->left);
        }
        if(node->right) {
            updateHeightRec(node->right);
        }

        updateHeight(node);
    }

    void rebalanceTree(Node* node, Node* maxParent) {
        auto leftHeight = this->getHeight(node->left);
        auto rightHeight = this->getHeight(node->right);

        if(rightHeight - leftHeight >= 2) { // right heavy
            auto rn = node->right;
            if(this->getHeight(rn->left) > this->getHeight(rn->right)) { // right-left
                this->rotateRight(rn);
                this->updateHeight(rn);
            }
            this->rotateLeft(node); // right-right, or second part of right-left
        } else if (leftHeight - rightHeight >= 2) { // left heavy
            auto ln = node->left;
            if(this->getHeight(ln->right) > this->getHeight(ln->left)) { // left-right
                this->rotateLeft(ln);
                this->updateHeight(ln);
            }
            this->rotateRight(node); // left-left, or second part of left-right
        }

        this->updateHeight(node);

        if(node->parent != nullptr && node != maxParent) {
            this->rebalanceTree(node->parent, maxParent);
        }
    }

    void printDotFileRec(std::ofstream& outfile, Node* node) {
        if(node->left) {
            outfile << node->value << " -> " << node->left->value << ";" << std::endl;
            printDotFileRec(outfile, node->left);
        }
        if(node->right) {
            outfile << node->value << " -> " << node->right->value << ";" << std::endl;
            printDotFileRec(outfile, node->right);
        }
    }
    omp_lock_t rootInsertLock;

public:
    AvlTreePar() {
        this->root = nullptr;
    }

    ~AvlTreePar() {

        // TODO: clear tree data :)
    }

    void insert(T elem) {

        Node* newElem = new Node{};
        newElem->value = elem;
        newElem->parent = nullptr;
        newElem->left = nullptr;
        newElem->right = nullptr;
        newElem->height = 0;
        bool retry = false;


        // if the element is the first node, set it as root
        if(this->root == nullptr) {
            rootlock.lock();
            {
                if (this->root == nullptr) {
                    this->root = newElem;
                    rootlock.unlock();
                    return;
                }
            }
            rootlock.unlock();
        }
        Node* lastUnequalNode;
        Node* lockedNode;

        do {
            lastUnequalNode = nullptr;
            if(!findFirstUnequalNode(root, newElem, &lastUnequalNode)) {
                return;
            }

            Node* parent = lastUnequalNode->parent;
            if(parent != nullptr) {
                parent->rwlock.lock();
                lockedNode = parent;
                // std::cerr << "Write Locked " << &parent->rwlock << std::endl;
                writeLocked = true;
                if(parent->left != lastUnequalNode && parent->right != lastUnequalNode) {
                    parent->rwlock.unlock();
                    // std::cerr << "Write Unlocked " << &parent->rwlock << std::endl;
                    writeLocked = false;
                    retry = true;
                } else {
                    retry = false;
                }
            } else {
                lastUnequalNode->rwlock.lock();
                lockedNode = lastUnequalNode;
                //  std::cerr << "Write Locked " << &lastUnequalNode->rwlock << std::endl;
                writeLocked = true;
                if (lastUnequalNode->parent != nullptr) {
                    lastUnequalNode->rwlock.unlock();
                    // std::cerr << "Write Unlocked " << &lastUnequalNode->rwlock << std::endl;
                    writeLocked = false;
                    retry = true;
                } else {
                    retry = false;
                }
            }

            if(!retry &&
                (lastUnequalNode->left != nullptr || lastUnequalNode->right != nullptr) &&
                !isUnequalNode(lastUnequalNode)) {
                lockedNode->rwlock.unlock();
                //std::cerr << "Write Unlocked " << &lockedNode->rwlock << std::endl;
                writeLocked = false;
                retry = true;
            }
        } while(retry);

        if(!this->insertRec(lastUnequalNode, newElem)) {
            // element was already in the tree
            // std::cerr << "Write Unlocked " << &lockedNode->rwlock << std::endl;
            lockedNode->rwlock.unlock();
            return;
        }

        // ensure AVL property
        this->rebalanceTree(newElem, lastUnequalNode);
        //this->printDotFile("test.dot");

        lockedNode->rwlock.unlock();
        //std::cerr << "Write Unlocked " << &lockedNode->rwlock << std::endl;
        writeLocked = false;
    }

    void getData(Node* node, std::vector<Node*>& data) {
        if (node == nullptr) {
            return;
        }
        getData(node->left, data);
        data.push_back(node);
        getData(node->right, data);
    }

    bool verifyItems(T* array, int size) {
        auto treeData = std::vector<Node*>();
        treeData.reserve(size);
        getData(this->root, treeData);
        int idx = 0;
        bool isBalanced = true;
        bool isValidOrder = true;

        for(auto node : treeData) {
            if(isBalanced && abs(this->getHeight(node->left) - this->getHeight(node->right)) > 1) {
                std::cout << "Tree is not balanced" << std::endl;
                isBalanced = false;
            }
            if(isValidOrder && node->value != array[idx]) {
                std::cout << "Tree value " << std::dec << node->value << " is not equal to " << array[idx] << " (idx: "<< idx << ") from verfication values" << std::endl;
                isValidOrder = false;
            }
            while(node->value == array[idx]) {
                idx++;
            }
        }

        return isBalanced && isValidOrder;
    }

    void printDotFile(const std::string filename) {
        std::ofstream outfile (filename);

        outfile << "digraph avltree" << std::endl;
        outfile << "{" << std::endl;

        printDotFileRec(outfile, this->root);

        outfile << "}" << std::endl;

        outfile.close();
    }


};


#endif //EXERCISE6_AVL_PAR_H
